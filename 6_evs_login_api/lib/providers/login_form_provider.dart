import 'package:flutter/material.dart';

class LoginFormProvider extends ChangeNotifier {
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  String usuario = '';
  String password = '';
  int telefono = 0;

  bool _isLoading = false;
  bool _getDatos = false;
  bool get isLoading => _isLoading;

  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  bool isValidForm() {
    print(formKey.currentState?.validate());

    print('$usuario - $password - $telefono');

    return formKey.currentState?.validate() ?? false;
  }
}
