import 'dart:io';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:tienda_evs_4/presentation/screens/Widgets/lista_producto_seleccionados.dart';
import 'package:tienda_evs_4/providers/tienda_provider.dart';
import 'package:tienda_evs_4/utils/grabar_memoria.dart';

class CarritoScreen extends StatelessWidget {
  const CarritoScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Mi carrito"),
          centerTitle: true,
        ),
        body: const ListaProductoSelecciondos(),
        bottomNavigationBar: Container(
          height: 70,
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          color: Colors.blue,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: [
              ElevatedButton(
                onPressed: () async {
                  GrabarMemoria.grabarMIP('MIP', context);
                },
                child: const Text('Memoria Interna Privada'),
              ),
              const SizedBox(
                width: 5,
              ),
              ElevatedButton(
                onPressed: () {
                  GrabarMemoria.grabarMEP('MEP', context);
                },
                child: const Text('Memoria Externa Privada'),
              ),
              const SizedBox(
                width: 5,
              ),
              ElevatedButton(
                onPressed: () {
                  GrabarMemoria.grabarCarpetaDocuments('CD', context);
                },
                child: const Text('Carpeta Documents'),
              ),
            ],
          ),
        ));
  }
}
