import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tienda_evs_4/model/producto.dart';
import 'package:tienda_evs_4/presentation/screens/Widgets/card_producto_carrito.dart';
import 'package:tienda_evs_4/providers/tienda_provider.dart';

class ListaProductoSelecciondos extends StatelessWidget {
  const ListaProductoSelecciondos({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 10,
      ),
      child: Column(
        children: [
          Expanded(child: Consumer<TiendaProvider>(
            builder: (context, provider, child) {
              if (provider.productosSeleccionados.isEmpty) {
                return const Center(
                  child: Text("Carrito vacio"),
                );
              } else {
                return ListView.builder(
                  itemCount: provider.productosSeleccionados.length,
                  itemBuilder: (context, index) {
                    Producto producto = provider.productosSeleccionados.toList()[index];
                    return CardProductoCarrito(producto: producto, tiendaProvider: provider);
                  },
                );
              }
            },
          ))
        ],
      ),
    ));
  }
}
