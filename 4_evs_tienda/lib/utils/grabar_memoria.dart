import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:tienda_evs_4/providers/tienda_provider.dart';

class GrabarMemoria {
  static Future<PermissionStatus> validarPermisos() async {
    PermissionStatus status = await Permission.manageExternalStorage.status;
    // Si el permiso no está concedido, solicitarlo al usuario
    if (!status.isGranted) {
      status = await Permission.manageExternalStorage.request();
    }
    return status;
  }

  static Future<void> grabarCarrito(BuildContext context, Directory? directory, String option) async {
    if (directory != null) {
      File file = File('${directory.path}/mi_archivo_$option.txt');

      // Escribir el texto en el archivo
      final tiendaProvider = Provider.of<TiendaProvider>(context, listen: false);
      await file.writeAsString(tiendaProvider.productosSeleccionados.toString());

      // Mostrar un mensaje de éxito
      // ignore: use_build_context_synchronously
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Archivo guardado con éxito en ${file.path}'),
      ));
    } else {
      // Mostrar un mensaje si no se pudo obtener el directorio externo
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('No se pudo obtener el directorio externo'),
      ));
    }
  }

  static Future<void> grabarMIP(String option, BuildContext context) async {
    PermissionStatus status = await validarPermisos();
    // Verificar y solicitar permiso de almacenamiento si es necesario
    if (status.isGranted) {
      // Obtener el directorio de almacenamiento local
      Directory? directory = await getApplicationDocumentsDirectory();
      // Crear el archivo en el directorio
      // ignore: use_build_context_synchronously
      await grabarCarrito(context, directory, option);
    } else {
      // Mostrar un mensaje si se deniega el permiso
      // ignore: use_build_context_synchronously
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('Permiso de almacenamiento denegado'),
      ));
    }
  }

  static Future<void> grabarMEP(String option, BuildContext context) async {
    PermissionStatus status = await validarPermisos();
    // Verificar y solicitar permiso de almacenamiento si es necesario
    if (status.isGranted) {
      // Obtener el directorio de almacenamiento externo
      Directory? directory = await getExternalStorageDirectory();
      // Crear el archivo en el directorio externo
      // ignore: use_build_context_synchronously
      await grabarCarrito(context, directory, option);
    } else {
      // Mostrar un mensaje si se deniega el permiso
      // ignore: use_build_context_synchronously
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('Permiso de almacenamiento denegado'),
      ));
    }
  }

  static Future<void> grabarCarpetaDocuments(String option, BuildContext context) async {
    // Verificar y solicitar permiso de almacenamiento externo si es necesario
    PermissionStatus status = await validarPermisos();
    // Verificar y solicitar permiso de almacenamiento si es necesario
    if (status.isGranted) {
      // Crear el referencia al directoruio Documents
      Directory? directory = Directory("/storage/self/primary/Documents");
      // ignore: use_build_context_synchronously
      await grabarCarrito(context, directory, option);
    } else {
      // Mostrar un mensaje si se deniega el permiso
      // ignore: use_build_context_synchronously
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('Permiso de almacenamiento denegado'),
      ));
    }
  }
}
