import 'package:flutter/material.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:video_player/video_player.dart';
import 'dart:io';

class CapturarVideoScreen extends StatefulWidget {
  const CapturarVideoScreen({super.key});

  @override
  CapturarVideoState createState() => CapturarVideoState();
}

class CapturarVideoState extends State<CapturarVideoScreen> {
  File? _video;
  VideoPlayerController? _controller;

  Future<void> _capturarVideo() async {
    final picker = ImagePicker();
    final videoCreado = await picker.pickVideo(source: ImageSource.camera);

    if (videoCreado != null) {
      _video = File(videoCreado.path);
      _controller = VideoPlayerController.file(_video!)
        ..initialize().then((_) {
          setState(() {});
          _controller!.play();
        });
    }
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  Future<void> _grabarEnGaleria() async {
    if (_video != null) {
      PermissionStatus status = await Permission.storage.request();
      if (!status.isGranted) {
        status = await Permission.manageExternalStorage.request();
      }
      if (status.isGranted) {
        final result = await ImageGallerySaver.saveFile(_video!.path);

        // ignore: use_build_context_synchronously
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(result['isSuccess'] ? 'Grabado con exito!' : 'Error al grabar.')),
        );
      } else {
        // ignore: use_build_context_synchronously
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Permiso denegado')),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Capturar video')),
      body: Center(
        child: _controller == null
            ? const Text('No es posible grabar video.')
            : _controller!.value.isInitialized
                ? AspectRatio(
                    aspectRatio: _controller!.value.aspectRatio,
                    child: VideoPlayer(_controller!),
                  )
                : const CircularProgressIndicator(),
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            onPressed: _capturarVideo,
            tooltip: 'Grabar video',
            child: const Icon(Icons.videocam),
          ),
          const SizedBox(height: 10),
          FloatingActionButton(
            onPressed: _grabarEnGaleria,
            tooltip: 'Grabar en galeria',
            child: const Icon(Icons.save),
          ),
        ],
      ),
    );
  }
}
