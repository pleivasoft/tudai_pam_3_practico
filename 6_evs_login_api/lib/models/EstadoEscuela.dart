import 'package:intl/intl.dart';

class EstadoEscuela {
  int? escuelaEstadoId;
  String? descripcion;
  bool estado = false;
  DateTime? fecha;

  String get strFecha => DateFormat('dd/MM/yyyy HH:mm').format(fecha!);

  EstadoEscuela.empty();
  EstadoEscuela(
      {this.escuelaEstadoId,
      this.descripcion,
      required this.estado,
      this.fecha});

  EstadoEscuela.fromJson(Map<String, dynamic> json) {
    escuelaEstadoId = json['escuelaEstadoId'];
    descripcion = json['descripcion'];
    estado = json['estado'];
    fecha = json['fecha'] == null ? null : DateTime.tryParse(json['fecha']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['escuelaEstadoId'] = this.escuelaEstadoId;
    data['descripcion'] = this.descripcion;
    data['estado'] = this.estado;
    data['fecha'] = this.fecha;
    return data;
  }
}
