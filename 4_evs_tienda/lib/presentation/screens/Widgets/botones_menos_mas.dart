import 'package:flutter/material.dart';

class BotonesMenosMas extends StatelessWidget {
  final VoidCallback restarCantidad;
  final VoidCallback agregarCantidad;
  final String text;
  const BotonesMenosMas({super.key, required this.agregarCantidad, required this.restarCantidad, required this.text});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        IconButton(onPressed: restarCantidad, icon: const Icon(Icons.remove)),
        Text(text),
        IconButton(onPressed: agregarCantidad, icon: const Icon(Icons.add)),
      ],
    );
  }
}
