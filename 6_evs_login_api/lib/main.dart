import 'package:app_policia/page/electoral/asistencia_page.dart';
import 'package:app_policia/page/electoral/carga_incidente.dart';
import 'package:app_policia/page/electoral/estado_page.dart';
import 'package:app_policia/page/electoral/listado_escuela_page.dart';
import 'package:app_policia/page/login/validate_code_sms_page.dart';
import 'package:app_policia/services/elecciones_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:app_policia/screens/screens.dart';
import 'package:app_policia/services/services.dart';

final navigatorKey = GlobalKey<NavigatorState>();
void main() => runApp(AppState());

class AppState extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AuthService()),
        ChangeNotifierProvider(create: (_) => EleccionesService()),
      ],
      child: MyApp(),
    );
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: navigatorKey,
      debugShowCheckedModeBanner: false,
      title: 'App Policia',
      initialRoute: 'checking',
      routes: {
        'checking': (_) => CheckAuthScreen(),
        'home': (_) => HomeScreen(),
        'product': (_) => ProductScreen(),
        'login': (_) => LoginScreen(),
        'register': (_) => RegisterScreen(),
        'validatecode': (_) => ValidateCodesmsPage(),
        'asistencia': (_) => AsistenciaPage(),
        'estadoElecciones': (_) => EstadoEleccionesPage(),
        'incidente': (_) => IncidenciaPage(),
        'listadoescuelas': (_) => ListadoEscuelasPage(),
      },
      scaffoldMessengerKey: NotificationsService.messengerKey,
      theme: ThemeData.light().copyWith(
          hintColor: Colors.white,
          scaffoldBackgroundColor: Colors.grey[20],
          appBarTheme: AppBarTheme(elevation: 0, color: Colors.indigo),
          floatingActionButtonTheme: FloatingActionButtonThemeData(
              backgroundColor: Colors.indigo, elevation: 0)),
    );
  }
}
