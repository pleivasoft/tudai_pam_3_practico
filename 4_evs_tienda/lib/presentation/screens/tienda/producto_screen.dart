import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tienda_evs_4/presentation/screens/Widgets/agregar_producto_form.dart';
import 'package:tienda_evs_4/providers/productos_provider.dart';

class ProductoScreen extends StatelessWidget {
  const ProductoScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final provider = context.watch<ProductosProvider>();
    provider.cargarProductos();
    return Scaffold(
        appBar: AppBar(
          title: const Text('Productos'),
        ),
        body: ListView.builder(
          itemCount: provider.productos.length,
          itemBuilder: (context, index) {
            final producto = provider.productos[index];
            return ListTile(
              title: Text(producto.nombre),
              subtitle: Text('Precio: ${producto.precio}, Cantidad: ${producto.cantidad.value}'),
              trailing: IconButton(
                icon: const Icon(Icons.delete),
                onPressed: () {
                  provider.eliminarProducto(producto.id);
                },
              ),
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.add),
          onPressed: () {
            showDialog(context: context, builder: (context) => const AgregarProductoForm());
          },
        ));
  }
}
