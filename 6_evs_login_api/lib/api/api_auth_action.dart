import 'package:app_policia/api/api_auth_provider.dart';
import 'package:app_policia/models/login/login_body.dart';
import 'package:app_policia/models/refreshtoken/refresh_token_body.dart';
import 'package:app_policia/models/token/token.dart';

class ApiAuthAction {
  final ApiAuthProvider _apiAuthProvider = ApiAuthProvider();

  Future<Token> postLoginUser(LoginBody loginBody) =>
      _apiAuthProvider.login(loginBody);

  Future<Token> postRefreshAuth(RefreshTokenBody refreshTokenBody) =>
      _apiAuthProvider.refreshAuth(refreshTokenBody);

  void postSetTokenNotificacion(LoginBody loginBody) =>
      _apiAuthProvider.setTokenNotificacion(loginBody);
}
