import 'package:flutter/material.dart';

class NotificationsService {
  static GlobalKey<ScaffoldMessengerState> messengerKey =
      new GlobalKey<ScaffoldMessengerState>();

  static showSnackbar(String message) {
    final snackBar = new SnackBar(
      content:
          Text(message, style: TextStyle(color: Colors.white, fontSize: 20)),
      behavior: SnackBarBehavior.floating,
    );

    messengerKey.currentState!.showSnackBar(snackBar);
  }
  static showSnackbarError(String message) {
    final snackBar = new SnackBar(
      backgroundColor: Colors.red,
      content:
          Text(message, style: TextStyle(color: Colors.white, fontSize: 20)),
      behavior: SnackBarBehavior.floating,
    );

    messengerKey.currentState!.showSnackBar(snackBar);
  }
}
