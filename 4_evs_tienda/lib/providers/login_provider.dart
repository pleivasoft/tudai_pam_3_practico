import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

// Definimos un modelo para el login
class LoginProvider extends ChangeNotifier {
  String _email = '';
  bool _isLoggedIn = false;

  // Constructor
  LoginProvider() {
    _cargaSession();
  }

  // Getter para el email
  String get email => _email;

  // Getter para el estado de la sesión
  bool get isLoggedIn => _isLoggedIn;

  // Método para iniciar sesión
  Future<void> login(String email, String password) async {
    // Validación simple de credenciales (hardcoded)
    if (email == 'usuario@iua.edu.ar' && password == '123456') {
      _email = email;
      _isLoggedIn = true;

      // Guardar la sesión en SharedPreferences
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString('email', email);
      await prefs.setBool('isLoggedIn', true);
      notifyListeners();
    } else {
      throw Exception('Credenciales incorrectas');
    }
  }

  // Método para cerrar sesión
  Future<void> logout() async {
    _email = '';
    _isLoggedIn = false;
    // Borrar la sesión de SharedPreferences
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove('email');
    await prefs.remove('isLoggedIn');
    notifyListeners();
  }

  // Método privado para cargar la sesión desde SharedPreferences
  Future<void> _cargaSession() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _email = prefs.getString('email') ?? '';
    _isLoggedIn = prefs.getBool('isLoggedIn') ?? false;
    notifyListeners();
  }
}
