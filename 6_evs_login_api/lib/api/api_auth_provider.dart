import 'dart:io';
import 'package:app_policia/utils/dio_logging_interceptors.dart';
import 'package:flutter/material.dart';
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';

import 'package:app_policia/api/environment.dart';
import 'package:app_policia/models/login/login_body.dart';
import 'package:app_policia/models/refreshtoken/refresh_token_body.dart';
import 'package:app_policia/models/token/token.dart';

class ApiAuthProvider {
  final Dio _dio = new Dio();

  final String clientId = 'bengkel-robot-client';
  final String clientSecret = 'bengkel-robot-secret';

  ApiAuthProvider() {
    (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    _dio.options.baseUrl = Environment.apiUrl;
    _dio.options.receiveDataWhenStatusError = true;
    _dio.options.connectTimeout = 100 * 100;
    _dio.options.receiveTimeout = 10 * 1000;
    _dio.interceptors.add(DioLoggingInterceptors(_dio));
  }

  //`,'Authorization': 'Basic ${base64Encode(utf8.encode('$clientId:$clientSecret'),)}',
  Future<Token> login(LoginBody loginBody) async {
    //loginBody.username = clientId;
    //loginBody.password = clientSecret;
    var formData = loginBody.toJson(); //FormData.fromMap(loginBody.toJson());
    print("--- updated user info: $formData");
    try {
      final response = await _dio.post(
        '/auth/login',
        data: loginBody.toJson(),
        options: Options(
          headers: {
            'Content-Type': 'application/json',
            //'requiresToken': true,
          },
        ),
      );
      return Token.fromJson(response.data);
    } catch (error, stacktrace) {
      var err = error as DioError;
      _printError(error, stacktrace);
      return Token.withError(
          err.response!.data["message"] != null ? err.response?.data["message"] : err.message);
    }
  }

  Future<Token> refreshAuth(RefreshTokenBody refreshTokenBody) async {
    try {
      final response = await _dio.post(
        '/auth/refresh-token',
        data: refreshTokenBody.toJson(),
        options: Options(
          headers: {
            'Content-Type': 'application/json',
            'requiresToken': false,
            /* 'Authorization': 'Basic ${base64Encode(
              utf8.encode('$clientId:$clientSecret'),
            )}', */
          },
        ),
      );
      return Token.fromJson(response.data);
    } catch (error, stacktrace) {
      _printError(error, stacktrace);
      return Token.withError('$error');
    }
  }

  Future<Token> setTokenNotificacion(LoginBody loginBody) async {
    try {
      final response = await _dio.post(
        '/auth/login/setTokenNotificacion',
        data: loginBody.toJson(),
        options: Options(
          headers: {
            'Content-Type': 'application/json',
            'requirestoken': true,
          },
        ),
      );
      return Token.fromJson(response.data);
    } catch (error, stacktrace) {
      _printError(error, stacktrace);
      return Token.withError('$error');
    }
  }

  void _printError(error, StackTrace stacktrace) {
    debugPrint('error: $error & stacktrace: $stacktrace');
  }
}
