import 'package:app_policia/models/escuela.dart';
import 'package:app_policia/services/elecciones_service.dart';
import 'package:app_policia/utils/dialog_estado.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class DrawerEscuelaEleccion extends StatelessWidget {
  const DrawerEscuelaEleccion(
      {Key? key,
      required this.escuela,
      required this.registrando,
      required this.context})
      : super(key: key);
  final Escuela? escuela;
  final BuildContext context;
  final bool registrando;
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          Expanded(
            child: ListView(
              padding: EdgeInsets.zero,
              children: <Widget>[
                DrawerHeader(
                  child: Text(
                    escuela!.escuela,
                    style: TextStyle(
                      fontSize: 20,
                      letterSpacing: 1,
                      color: Colors.yellow[700],
                    ),
                  ),
                  decoration: BoxDecoration(
                    color: Colors.blue[900],
                  ),
                ),
                ListTile(
                  leading: Icon(
                    Icons.done_all_outlined,
                    color: Colors.green,
                  ),
                  title: Text('Dar presente'),
                  onTap: () async {
                    Navigator.pop(context);
                    await Navigator.pushNamed(context, "asistencia");
                  },
                ),
                ListTile(
                  leading: Icon(
                    Icons.settings,
                    color: Colors.blue,
                  ),
                  title: Text('Cambiar estado'),
                  onTap: () async {
                    Navigator.pop(context);
                    await Navigator.pushNamed(context, "estadoElecciones");
                  },
                ),
                ListTile(
                  leading: Icon(
                    Icons.room,
                    color: Colors.deepPurpleAccent,
                  ),
                  title: Text('Ver en Mapa'),
                  onTap: () async {
                    await _launchMap();
                    Navigator.pop(context);
                  },
                ),
                Visibility(
                  visible: false,
                  child: ListTile(
                    leading: Icon(
                      Icons.person_pin,
                      color: Colors.amber,
                    ),
                    title: Text('Perfil'),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
                SizedBox(height: 30),
              ],
            ),
          ),
          Visibility(
            visible: false,
            child: ListTile(
              leading: Icon(
                Icons.power_settings_new_outlined,
                color: Colors.red,
              ),
              title: Text('Finalizar servicio'),
              onTap: () async {
                try {
                  await DialogEstado.showAlertDialog(context, 8, registrando);
                } catch (e) {}
                Navigator.pop(context);
              },
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _launchMap() async {
    EleccionesService eleccionesService =
        Provider.of<EleccionesService>(context, listen: false);
    String lat = eleccionesService.escuela!.latitud.toString();
    String lng = eleccionesService.escuela!.longitud.toString();

    final String gMlatlngnUrl =
        "https://www.google.com/maps/search/?api=1&query=${lat},${lng}";
    final Uri _url = Uri.parse(gMlatlngnUrl);
    if (!await launchUrl(_url)) {
      throw Exception('Could not launch $_url');
    }
  }
}
/*

 Drawer drawerElecciones() => Drawer(
        child: Column(
          children: [
            Expanded(
              child: ListView(
                padding: EdgeInsets.zero,
                children: <Widget>[
                  DrawerHeader(
                    child: Text(
                      escuela!.escuela,
                      style: TextStyle(
                        fontSize: 20,
                        letterSpacing: 1,
                        color: Colors.yellow[700],
                      ),
                    ),
                    decoration: BoxDecoration(
                      color: Colors.blue[900],
                    ),
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.done_all_outlined,
                      color: Colors.green,
                    ),
                    title: Text('Dar presente'),
                    onTap: () async {
                      Navigator.pop(context);
                      await Navigator.pushNamed(context, "asistencia");
                    },
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.settings,
                      color: Colors.blue,
                    ),
                    title: Text('Cambiar estado'),
                    onTap: () async {
                      Navigator.pop(context);
                      await Navigator.pushNamed(context, "estadoElecciones");
                    },
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.room,
                      color: Colors.deepPurpleAccent,
                    ),
                    title: Text('Ver en Mapa'),
                    onTap: () async {
                      await _launchMap();
                      Navigator.pop(context);
                    },
                  ),
                  Visibility(
                    visible: false,
                    child: ListTile(
                      leading: Icon(
                        Icons.person_pin,
                        color: Colors.amber,
                      ),
                      title: Text('Perfil'),
                      onTap: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                  SizedBox(height: 30),
                ],
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.power_settings_new_outlined,
                color: Colors.red,
              ),
              title: Text('Finalizar servicio'),
              onTap: () async {
                try {
                  await DialogEstado.showAlertDialog(context, 8, registrando);
                } catch (e) {}
                Navigator.pop(context);
              },
            ),
          ],
        ),
      );

*/ 