// To parse this JSON data, do
//
//     final escuela = escuelaFromJson(jsonString);

import 'dart:convert';

Escuela escuelaFromJson(String str) => Escuela.fromJson(json.decode(str));

String escuelaToJson(Escuela data) => json.encode(data.toJson());

class Escuela {
  Escuela({
    required this.id,
    required this.escuela,
    required this.dependencia,
    required this.estado,
    required this.mesas,
    required this.electores,
    required this.latitud,
    required this.longitud,
    required this.carga,
    required this.ultimaCarga,
  });
//
//xcopy  bin\Release\netcoreapp5.0\publish\*.* \\apmpol01\d$\AplicativosWebPPC\apppolicia
  int id;
  String escuela;
  String dependencia;
  String estado;
  String mesas;
  int electores;
  int ultimaCarga;
  double latitud;
  double longitud;
  List<Carga> carga;

  factory Escuela.fromJson(Map<String, dynamic> json) => Escuela(
      id: json["id"],
      escuela: json["escuela"],
      dependencia: json["dependencia"],
      estado: json["estado"],
      mesas: json["mesas"],
      electores: json["electores"],
      latitud: json["latitud"]?.toDouble(),
      longitud: json["longitud"]?.toDouble(),
      carga: List<Carga>.from(
        json["carga"].map((x) => Carga.fromJson(x)),
      ),
      ultimaCarga: json["ultimaCarga"]);

  Map<String, dynamic> toJson() => {
        "id": id,
        "escuela": escuela,
        "dependencia": dependencia,
        "estado": estado,
        "mesas": mesas,
        "electores": electores,
        "latitud": latitud,
        "longitud": longitud,
        "carga": List<dynamic>.from(carga.map((x) => x.toJson())),
        "ultimaCarga": ultimaCarga,
      };
}

class Carga {
  Carga({
    required this.id,
    required this.cantidad,
  });

  int id;
  int cantidad;

  factory Carga.fromJson(Map<String, dynamic> json) => Carga(
        id: json["id"],
        cantidad: json["cantidad"] ?? 0,
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "cantidad": cantidad,
      };
}
