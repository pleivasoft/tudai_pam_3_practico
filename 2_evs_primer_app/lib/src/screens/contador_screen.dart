import 'package:flutter/material.dart';
import 'package:primer_app/src/widget/custom_f_a_button.dart';

class ContadorScreen extends StatefulWidget {
  const ContadorScreen({super.key});

  @override
  State<ContadorScreen> createState() => _ContadorScreenState();
}

class _ContadorScreenState extends State<ContadorScreen> {
  int contador = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Contador'),
        actions: [
          IconButton(
              onPressed: () {
                setState(() {
                  contador = 0;
                });
              },
              icon: const Icon(Icons.refresh))
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('$contador', style: const TextStyle(fontSize: 160, fontWeight: FontWeight.w100)),
            Text('Click${contador == 1 ? '' : 's'}', style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w100))
          ],
        ),
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          CustomFAbutton(
            icono: Icons.refresh,
            onPress: () {
              setState(() {
                contador = 0;
              });
            },
          ),
          const SizedBox(
            height: 10,
          ),
          CustomFAbutton(
            icono: Icons.plus_one,
            onPress: () {
              setState(() {
                contador++;
              });
            },
          ),
          const SizedBox(
            height: 10,
          ),
          CustomFAbutton(
            icono: Icons.exposure_minus_1,
            onPress: () {
              setState(() {
                if (contador == 0) return;
                contador--;
              });
            },
          ),
        ],
      ),
    );
  }
}
