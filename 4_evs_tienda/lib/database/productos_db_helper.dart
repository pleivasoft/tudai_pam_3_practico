import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:tienda_evs_4/model/producto.dart';

class ProductosDBHelper {
  static Database? _database;
  static const _dbName = 'productos.db';
  static const _dbVersion = 2;
  static const _tableName = 'productos';

  static Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await initDB();
    return _database!;
  }

  static Future<Database> initDB() async {
    String path = join(await getDatabasesPath(), _dbName);

    print("pathDB: " + path);

    return await openDatabase(
      path,
      version: _dbVersion,
      onCreate: (db, version) async {
        await db.execute('''
          CREATE TABLE $_tableName(
            idProducto INTEGER PRIMARY KEY AUTOINCREMENT,
            id INTEGER ,
            nombre TEXT,
            precio INTEGER,
            cantidad INTEGER,
            icono TEXT
          )
        ''');
      },
      onUpgrade: (Database db, int oldVersion, int version) async {
        // Código para realizar las modificaciones en la base de datos
      },
    );
  }

  static Future<int> insertProducto(Producto producto) async {
    final db = await database;
    return await db.insert(_tableName, producto.toMap());
  }

  static Future<List<Producto>> getProductos() async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query(_tableName);
    return List.generate(maps.length, (i) {
      return Producto(
        id: maps[i]['idProducto'],
        nombre: maps[i]['nombre'],
        precio: maps[i]['precio'],
        cantidad: ValueNotifier<int>(maps[i]['cantidad']),
        icono: Icons.shopping_cart,
      );
    });
  }

  static Future<int> updateProducto(Producto producto) async {
    final db = await database;
    return await db.update(_tableName, producto.toMap(), where: 'idProducto = ?', whereArgs: [producto.id]);
  }

  static Future<int> deleteProducto(int id) async {
    final db = await database;
    return await db.delete(_tableName, where: 'idProducto = ?', whereArgs: [id]);
  }
}
