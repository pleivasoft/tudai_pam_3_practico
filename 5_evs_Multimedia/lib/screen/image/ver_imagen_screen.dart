import 'package:flutter/material.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'dart:io';
import 'package:permission_handler/permission_handler.dart';

class VerImagenScreen extends StatelessWidget {
  final String imagePath;
  const VerImagenScreen({super.key, required this.imagePath});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Vista previa')),
        // La imagen se almacena como un archivo en el dispositivo. Utilice el `Image.file`
        // constructor con la ruta dada para mostrar la imagen.
        body: Image.file(File(imagePath)),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: FloatingActionButton(
          onPressed: () => grabarImagen(context),
          child: const Icon(Icons.save),
        ));
  }

  Future<void> grabarImagen(BuildContext context) async {
    PermissionStatus status = await Permission.storage.request();
    if (!status.isGranted) {
      status = await Permission.manageExternalStorage.request();
    }
    if (status.isGranted) {
      final result = await ImageGallerySaver.saveFile(imagePath);

      // ignore: use_build_context_synchronously
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text(result['isSuccess'] ? 'Grabado con exito!' : 'Error al grabar.')),
      );
    } else {
      // ignore: use_build_context_synchronously
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Permiso denegado')),
      );
    }
  }
}
