import 'package:flutter/material.dart';
import 'package:telephony/telephony.dart';

onBackgroundMessage(SmsMessage message) {
  debugPrint("onBackgroundMessage called");
}

class ValidateCodesmsPage extends StatefulWidget {
  const ValidateCodesmsPage();

  @override
  State<ValidateCodesmsPage> createState() => _ValidateCodesmsPageState();
}

class _ValidateCodesmsPageState extends State<ValidateCodesmsPage> {
  String _message = "";
  bool _showClearButton = false;
  TextEditingController _textCodeController = new TextEditingController();
  final telephony = Telephony.instance;

  @override
  void initState() {
    super.initState();
    initPlatformState();
    _textCodeController.addListener(() {
      setState(() {
        _showClearButton = _textCodeController.text.length > 0;
      });
    });
  }

  onMessage(SmsMessage message) async {
    this._message = message.body ?? "Error reading message body.";
    print((message.address ?? "Error nro.") +
        " " +
        (message.body ?? "Error reading message body."));
  }

  onSendStatus(SendStatus status) {
    this._message = status == SendStatus.SENT ? "sent" : "delivered";
    print(status == SendStatus.SENT ? "sent" : "delivered");
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    // Platform messages may fail, so we use a try/catch PlatformException.
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.

    final bool? result = await telephony.requestPhoneAndSmsPermissions;

    if (result != null && result) {
      telephony.listenIncomingSms(
          onNewMessage: onMessage, onBackgroundMessage: onBackgroundMessage);
    }

    //if (!mounted) return;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Color.fromARGB(255, 2, 26, 49),
          title: Text("Validar de código ")),
      body: Container(
          padding: EdgeInsets.all(30.0),
          width: double.infinity,
          height: double.infinity,
          color: Color.fromARGB(255, 2, 26, 49),
          child: SingleChildScrollView(
            child: Stack(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "Ingrese el código de verificación que a continuación se envió por SMS a su teléfono móvil.",
                      style: TextStyle(color: Colors.white, fontSize: 18),
                    ),
                    SizedBox(height: 40),
                    TextField(
                      textAlign: TextAlign.right,
                      controller: _textCodeController,
                      style: TextStyle(
                        color: Color.fromARGB(255, 255, 255, 255),
                        fontSize: 40,
                      ),
                      textInputAction: TextInputAction.next,
                      autocorrect: false,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey),
                        ),
                        hintText: '',
                        labelText: '',
                        prefixIcon: Icon(
                          Icons.password_outlined,
                          color: Colors.grey,
                        ),
                        suffixIcon: _getClearButton(),
                      ),
                    ),
                    SizedBox(height: 50),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          flex: 1,
                          child: MaterialButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            disabledColor: Color.fromARGB(193, 133, 133, 133),
                            elevation: 0,
                            color: Colors.indigo,
                            child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 15),
                                child: Text(
                                  'Reenviar sms',
                                  style: TextStyle(color: Colors.white),
                                )),
                            onPressed: () {},
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          flex: 1,
                          child: MaterialButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            disabledColor: Color.fromARGB(193, 133, 133, 133),
                            elevation: 0,
                            color: Colors.green[700],
                            child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 15),
                                child: Text(
                                  'Confirma',
                                  style: TextStyle(color: Colors.white),
                                )),
                            onPressed: () {
                              print(_textCodeController.text);
                            },
                          ),
                        )
                      ],
                    )
                  ],
                )
              ],
            ),
          )),
    );
  }

  Widget? _getClearButton() {
    if (!_showClearButton) {
      return null;
    }

    return IconButton(
      padding: EdgeInsets.only(top: 20),
      onPressed: () => _textCodeController.clear(),
      icon: Icon(Icons.clear),
      color: Colors.white,
    );
  }
}
