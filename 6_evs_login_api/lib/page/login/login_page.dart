import 'package:app_policia/models/token/token.dart';
import 'package:flutter/material.dart';
import 'package:app_policia/providers/login_form_provider.dart';
import 'package:app_policia/services/services.dart';
import 'package:provider/provider.dart';
import 'package:app_policia/ui/input_decorations.dart';
import 'package:app_policia/widgets/widgets.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: AuthBackground(
            child: SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: 230),
          CardContainer(
              child: Column(
            children: [
              SizedBox(height: 10),
              ChangeNotifierProvider(
                  create: (_) => LoginFormProvider(), child: _LoginForm())
            ],
          )),
          SizedBox(height: 50),
          Align(
              alignment: Alignment.bottomCenter,
              child: Text(
                "Version: 1.0.3",
                style:
                    TextStyle(color: Colors.amber, fontWeight: FontWeight.bold),
              ))
        ],
      ),
    )));
  }
}

class _LoginForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final loginForm = Provider.of<LoginFormProvider>(context);

    return Container(
      child: Form(
        key: loginForm.formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Column(
          children: [
            TextFormField(
              initialValue: "",
              textInputAction: TextInputAction.next,
              autocorrect: false,
              keyboardType: TextInputType.number,
              decoration: InputDecorations.authInputDecoration(
                  hintText: '',
                  labelText: 'DNI',
                  prefixIcon: Icons.person_outline),
              onChanged: (value) => loginForm.usuario = value,
              validator: (value) {
                String pattern = r'^[\d]{1,2}\.?[\d]{3,3}\.?[\d]{3,3}$';
                RegExp regExp = new RegExp(pattern);
                return regExp.hasMatch(value ?? '')
                    ? null
                    : 'El valor ingresado no luce como dni';
              },
            ),
            SizedBox(height: 30),
            TextFormField(
              initialValue: "",
              autocorrect: false,
              obscureText: true,
              textInputAction: TextInputAction.next,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecorations.authInputDecoration(
                  hintText: '*****',
                  labelText: 'Contraseña',
                  prefixIcon: Icons.lock_outline),
              onChanged: (value) => loginForm.password = value,
              validator: (value) {
                return (value != null && value.length >= 1)
                    ? null
                    : 'Ingrese contraseña';
              },
            ),
            SizedBox(height: 30),
            TextFormField(
              initialValue: "",
              textInputAction: TextInputAction.go,
              autocorrect: false,
              keyboardType: TextInputType.phone,
              decoration: InputDecorations.authInputDecoration(
                  hintText: '',
                  labelText: 'Nro. telefono CAT',
                  prefixIcon: Icons.phone_android),
              onChanged: (value) =>
                  loginForm.telefono = int.tryParse(value) ?? 0,
              validator: (value) {
                return (value != null && value.length >= 10)
                    ? null
                    : 'Número de telefono no válido';
              },
            ),
            SizedBox(height: 30),
            MaterialButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                disabledColor: Colors.grey,
                elevation: 0,
                color: Colors.indigo,
                child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 80, vertical: 15),
                    child: Text(
                      loginForm.isLoading ? 'Validando ...' : 'Ingresar',
                      style: TextStyle(color: Colors.white),
                    )),
                onPressed: loginForm.isLoading
                    ? null
                    : () async {
                        FocusScope.of(context).unfocus();
                        final authService =
                            Provider.of<AuthService>(context, listen: false);

                        if (!loginForm.isValidForm()) return;

                        loginForm.isLoading = true;

                        // TODO: validar si el login es correcto
                        Token token = await authService.loginDevice(
                            loginForm.usuario,
                            loginForm.password,
                            loginForm.telefono);
                        /* Token token = await authService.loginDevice(
                            "29208339", "ASASASASA", 3516618242); */

                        if (token.error == null || token.error.isEmpty) {
                          loginForm.isLoading = false;
                          Navigator.pushReplacementNamed(
                              context, 'listadoescuelas');

/*                       final eleccionesService = Provider.of<EleccionesService>(context, listen: false);

                          try {
                          bool tieneVarias =  await eleccionesService.cargarEscuela();
                          loginForm.isLoading = false;
                          if (eleccionesService.escuelas.length>1) {
                            Navigator.pushReplacementNamed(
                                context, 'listadoescuelas');
                          } else
                            Navigator.pushReplacementNamed(context, 'home');
                          } catch (e) {
                            loginForm.isLoading = false;
                            NotificationsService.showSnackbarError(e.toString());
                          }*/
                        } else {
                          NotificationsService.showSnackbar(token.error);
                          loginForm.isLoading = false;
                        }
                      }),
          ],
        ),
      ),
    );
  }
}
