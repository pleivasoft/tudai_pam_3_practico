import 'package:app_policia/api/api_service.dart';
import 'package:app_policia/models/EstadoEscuela.dart';
import 'package:app_policia/models/turno.dart';
import 'package:app_policia/models/empleado.dart';
import 'package:app_policia/models/escuela.dart';
import 'package:app_policia/models/response.dart';

class ApiAction {
  final ApiService _apiService = ApiService();

  Future<List<Escuela>> getEscuela() => _apiService.getEscuela();

  Future<bool> cargarNovedad(body) => _apiService.cargarNovedad(body);

  Future<Empleado?> buscaEmpleado(dni) => _apiService.buscarEmpleado(dni);

  Future<List<Empleado>> obtenerPersonalEscuelaPorDni(dni) => _apiService.obtenerPersonalEscuelaPorDni(dni);

  Future<List<Turno>> obtenerTurnos() => _apiService.obtenerTurnos();

  Future<bool> registrarAsistencia(body) =>
      _apiService.registrarAsistencia(body);

  Future<bool> registrarIncidente(body) => _apiService.registrarIncidente(body);

  /*
  Gestion estados de eleccion
  */
  Future<List<EstadoEscuela>> obtenerEstados(escuelaId) =>
      _apiService.obtenerEstados(escuelaId);

  Future<Respuesta> registrarEstado(body) => _apiService.registrarEstado(body);
}
