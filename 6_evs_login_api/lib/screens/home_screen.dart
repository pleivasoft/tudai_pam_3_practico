import 'package:app_policia/widgets/detalle_escuela.dart';
import 'package:app_policia/widgets/drawer_escuela_eleccion.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:app_policia/api/api_service.dart';
import 'package:app_policia/widgets/AnimatedAppBar.dart';
import 'package:app_policia/models/escuela.dart';
import 'package:app_policia/services/elecciones_service.dart';
import 'package:app_policia/services/services.dart';
import 'package:app_policia/utils/palette.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  // ignore: non_constant_identifier_names
  late AnimationController _ColorAnimationController;
  late EleccionesService eleccionesService;
  Escuela? escuela;
  // ignore: non_constant_identifier_names
  late Animation _colorTween, _homeTween, _workOutTween, _iconTween, _drawerTween;

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  TextEditingController _searchController = new TextEditingController();

  bool show = false;

  bool displayCarga = false;

  bool registrando = false;

  bool cantidadCorrecta = false;

  get textStyle => TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
        color: Color.fromARGB(255, 255, 255, 255),
      );

  get textStyleTitulo => TextStyle(
        fontSize: 16,
        letterSpacing: 2,
        color: Colors.yellow[700],
      );

  @override
  void initState() {
    _ColorAnimationController = AnimationController(vsync: this, duration: Duration(seconds: 0));
    _colorTween = ColorTween(begin: Colors.transparent, end: Colors.white).animate(_ColorAnimationController);
    _iconTween = ColorTween(begin: Colors.white, end: Colors.lightBlue.withOpacity(0.5)).animate(_ColorAnimationController);
    _drawerTween = ColorTween(begin: Colors.white, end: Colors.black).animate(_ColorAnimationController);
    _homeTween = ColorTween(begin: Colors.white, end: Colors.blue).animate(_ColorAnimationController);
    _workOutTween = ColorTween(begin: Colors.white, end: Colors.black).animate(_ColorAnimationController);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    eleccionesService = Provider.of<EleccionesService>(context, listen: false);
    var authoService = Provider.of<AuthService>(context, listen: false);
    this.escuela = eleccionesService.escuela;

    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Palette.backgroundColor,
        drawer: DrawerEscuelaEleccion(
          escuela: this.escuela,
          registrando: this.registrando,
          context: context,
        ),
        body: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            child: Stack(
              children: [
                Positioned(
                  top: 0,
                  right: 0,
                  left: 0,
                  child: Container(
                    height: 400,
                    decoration: BoxDecoration(image: DecorationImage(image: AssetImage("assets/elecciones.jpg"), fit: BoxFit.fill)),
                    child: DetalleEscuela(
                        escuela: escuela,
                        textStyleTitulo: textStyleTitulo,
                        textStyle: textStyle,
                        eleccionesService: eleccionesService,
                        authoService: authoService),
                  ),
                ),
                AnimatedAppBar(
                  drawerTween: _drawerTween,
                  onPressed: () {
                    _scaffoldKey.currentState!.openDrawer();
                  },
                  colorAnimationController: _ColorAnimationController,
                  colorTween: _colorTween,
                  homeTween: _homeTween,
                  iconTween: _iconTween,
                  workOutTween: _workOutTween,
                ),
                //Main Contianer for Login and Signup
                Container(
                  margin: EdgeInsets.only(top: 260, left: 20, right: 20, bottom: 10),
                  padding: EdgeInsets.all(20),
                  decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(15), boxShadow: [
                    BoxShadow(color: Colors.black.withOpacity(0.3), blurRadius: 15, spreadRadius: 5),
                  ]),
                  child: Column(children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Column(
                          children: [
                            Text(
                              "Carga de votantes",
                              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Palette.activeColor),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 3),
                              height: 2,
                              width: 150,
                              color: Colors.orange,
                            )
                          ],
                        ),
                      ],
                    ),
                    Expanded(flex: 1, child: buildCargaElectores()),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.deepOrange[700], minimumSize: Size(double.infinity, 48) // put the width and height you want
                            ),
                        child: Text('Carga Incidente'),
                        onPressed: () async {
                          await Navigator.pushNamed(context, "incidente");
                        },
                      ),
                    ),
                    SizedBox(height: 10),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.amber[800], minimumSize: Size(double.infinity, 48) // put the width and height you want
                            ),
                        child: Text('Carga Excepcional'),
                        onPressed: () async {
                          cargarNovedad(99);
                        },
                      ),
                    )
                  ]),
                ),

                // Trick to add the submit button
              ],
            ),
          ),
        ));
  }

  Widget buildCargaElectores() {
    List<Carga> lista = escuela!.carga;
    displayCarga = false;

    return lista.isNotEmpty
        ? Consumer<EleccionesService>(
            builder: (context, value, child) {
              return RefreshIndicator(
                onRefresh: actualizar,
                child: ListView.builder(
                  shrinkWrap: false,
                  itemCount: lista.length,
                  itemBuilder: (BuildContext context, int index) {
                    Carga carga = lista[index];
                    if (index == 0 && carga.cantidad == 0) {
                      displayCarga = true;
                      return buildItemCarga(lista, carga, index);
                    }
                    if (index > 0 && carga.cantidad == 0 && displayCarga == false) {
                      displayCarga = true;
                      return buildItemCarga(lista, carga, index);
                    }
                    return Container();
                  },
                ),
              );
            },
          )
        : Center(
            child: Text("Sin datos"),
          );
  }

  ListTile buildItemCarga(List<Carga> lista, Carga carga, int index) {
    return ListTile(
      leading: const Icon(Icons.access_time_rounded),
      trailing: carga.cantidad == 0
          ? index == 0
              ? IconButton(onPressed: null, icon: Icon(Icons.add, color: Colors.green), color: Colors.green)
              : (lista[index - 1].cantidad > 0)
                  ? IconButton(
                      onPressed: null,
                      icon: Icon(
                        Icons.add,
                        color: Colors.green,
                      ),
                    )
                  : null
          : Text(
              carga.cantidad == 0 ? "" : carga.cantidad.toString(),
              style: TextStyle(color: Color.fromARGB(255, 20, 18, 121), fontSize: 30),
            ),
      title: Text(
        carga.id.toString() + ":00 hs.",
        style: TextStyle(fontSize: 20),
      ),
      onTap: () {
        if (index == 0 && carga.cantidad == 0)
          cargarNovedad(carga.id);
        else {
          if (carga.cantidad == 0 && (lista[index - 1].cantidad > 0)) cargarNovedad(carga.id);
        }
      },
    );
  }

  TextButton buildTextButton(IconData icon, String title, Color backgroundColor) {
    return TextButton(
      onPressed: () {},
      style: TextButton.styleFrom(
          side: BorderSide(width: 1, color: Colors.grey),
          minimumSize: Size(145, 40),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          backgroundColor: backgroundColor),
      child: Row(
        children: [
          Icon(
            icon,
          ),
          SizedBox(
            width: 5,
          ),
          Text(
            title,
          )
        ],
      ),
    );
  }

  Widget buildBottomHalfContainer(bool showShadow) {
    return AnimatedPositioned(
      duration: Duration(milliseconds: 700),
      curve: Curves.bounceInOut,
      top: 430,
      right: 0,
      left: 0,
      child: Center(
        child: Container(
          height: 90,
          width: 90,
          padding: EdgeInsets.all(15),
          decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(50), boxShadow: [
            if (showShadow)
              BoxShadow(
                color: Colors.black.withOpacity(.3),
                spreadRadius: 1.5,
                blurRadius: 10,
              )
          ]),
          child: !showShadow
              ? Container(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(colors: [Colors.orange, Colors.red], begin: Alignment.topLeft, end: Alignment.bottomRight),
                      borderRadius: BorderRadius.circular(30),
                      boxShadow: [BoxShadow(color: Colors.black.withOpacity(.3), spreadRadius: 1, blurRadius: 2, offset: Offset(0, 1))]),
                  child: Icon(
                    Icons.arrow_forward,
                    color: Colors.white,
                  ),
                )
              : Center(),
        ),
      ),
    );
  }

  Widget buildTextField(IconData icon, String hintText, bool isPassword, bool isEmail) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: TextField(
        obscureText: isPassword,
        keyboardType: isEmail ? TextInputType.emailAddress : TextInputType.text,
        decoration: InputDecoration(
          prefixIcon: Icon(
            icon,
            color: Palette.iconColor,
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Palette.textColor1),
            borderRadius: BorderRadius.all(Radius.circular(35.0)),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Palette.textColor1),
            borderRadius: BorderRadius.all(Radius.circular(35.0)),
          ),
          contentPadding: EdgeInsets.all(10),
          hintText: hintText,
          hintStyle: TextStyle(fontSize: 14, color: Palette.textColor1),
        ),
      ),
    );
  }

  cargarNovedad(hora) async {
    await showDialog(context: context, builder: (context) => dialog(context, hora));
    setState(() {
      cantidadCorrecta = false;
      registrando = false;
      _searchController.clear();
    });
  }

  StatefulBuilder dialog(BuildContext context, int hora) {
    return StatefulBuilder(builder: (context, setState) {
      return AlertDialog(
        title: Text(
          hora == 99 ? 'Cargar excepcional' : 'Cargar electores',
          style: TextStyle(color: hora == 99 ? Colors.amber[800] : Colors.blue),
        ),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            RichText(
              text: hora == 99
                  ? TextSpan(
                      text: 'Ingrese la cantidad de electores',
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.black,
                      ))
                  : TextSpan(
                      text: 'Ingrese la cantidad de electores de las',
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.black,
                      ),
                      children: [
                        TextSpan(
                          text: "$hora:00 hs.",
                          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.blue),
                        )
                      ],
                    ),
            ),
            SizedBox(height: 10),
            Form(
              autovalidateMode: AutovalidateMode.always,
              child: TextFormField(
                controller: _searchController,
                textInputAction: TextInputAction.next,
                textAlign: TextAlign.right,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Palette.textColor1),
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Palette.textColor1),
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                  contentPadding: EdgeInsets.only(right: 30),
                  prefixIcon: _searchController.text.isNotEmpty
                      ? InkWell(
                          onTap: () {
                            setState(() {
                              _searchController.clear();
                              cantidadCorrecta = false;
                            });
                          },
                          child: Icon(Icons.clear),
                        )
                      : null,
                ),
                validator: (value) {
                  try {
                    if (value != null && value.isNotEmpty) {
                      if (!isNumeric(value.toString())) return "Valor ingresado no es valido";
                      int max = this.escuela!.electores;
                      int mayorque = this.escuela!.ultimaCarga;

                      var listaaux = List<Carga>.from(escuela!.carga);
                      listaaux.sort((a, b) => a.cantidad.compareTo(b.cantidad));
                      Carga? carga = listaaux.last;
                      mayorque = mayorque > carga.cantidad ? mayorque : carga.cantidad;

                      if (!esCantidadCorrecta(value)) {
                        return 'Rango permitido ($mayorque, $max)';
                        //return 'El valor ingresado debe estar entre ($mayorque, $max)';
                      }
                    }
                    return null;
                  } catch (e) {
                    return null;
                  }
                },
                onChanged: (value) {
                  cantidadCorrecta = esCantidadCorrecta(value);
                  setState(() {});
                },
              ),
            ),
          ],
        ),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.pop(context, false);
            },
            child: const Text('Cancelar'),
          ),
          TextButton(
            onPressed: cantidadCorrecta
                ? registrando
                    ? null
                    : () async {
                        setState(() {
                          registrando = true;
                        });
                        try {
                          int cantidad = int.parse(_searchController.text);
                          var body = {
                            "escuelaId": escuela!.id,
                            "hora": hora,
                            "cantidad": cantidad,
                          };
                          var estado = await ApiService().cargarNovedad(body);
                          if (estado) eleccionesService.cargarCantidad(hora, cantidad);
                          setState(() {
                            _searchController.clear();
                          });
                          Navigator.pop(context, true);
                        } catch (e) {
                          NotificationsService.showSnackbar("Verifique cantidad...");
                        }
                      }
                : null,
            child: registrando
                ? SizedBox(
                    child: CircularProgressIndicator(),
                    height: 20,
                    width: 20,
                  )
                : const Text('Registrar'),
          ),
        ],
      );
    });
  }

  bool esCantidadCorrecta(String value) {
    try {
      if (value.isNotEmpty) {
        int cantidad = int.parse(value.toString());
        int max = this.escuela!.electores;
        int ultimaCarga = this.escuela!.ultimaCarga;
        int mayorque = 0;
        var listaaux = List<Carga>.from(escuela!.carga);
        listaaux.sort((a, b) => a.cantidad.compareTo(b.cantidad));
        Carga? carga = listaaux.last;
        mayorque = carga.cantidad;
        if (cantidad <= mayorque || cantidad >= max || cantidad <= ultimaCarga) {
          return false;
        } else {
          return true;
        }
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  Future<void> actualizar() async {
    await eleccionesService.refreshEscuelas();
    setState(() {});
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }
}
