import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tienda_evs_4/presentation/screens/login/login_screen.dart';
import 'package:tienda_evs_4/presentation/screens/tienda/tienda_screen.dart';
import 'package:tienda_evs_4/providers/login_provider.dart';
import 'package:tienda_evs_4/providers/productos_provider.dart';
import 'package:tienda_evs_4/providers/tienda_provider.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => TiendaProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => LoginProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => ProductosProvider(),
        )
      ],
      child: MaterialApp(
        title: 'Tienda App',
        debugShowCheckedModeBanner: false,
        home: Consumer<LoginProvider>(
          builder: (context, loginProvider, child) {
            return loginProvider.isLoggedIn ? const TiendaScreen() : const LoginScreen();
          },
        ),
      ),
    );
  }
}
/*
Consumer proporcionado por el paquete provider que se utiliza para escuchar y consumir un cambio en un modelo de datos específico. 
Se utiliza junto con Provider.of<T>(context) para reconstruir solo la parte del árbol de widgets que depende de los cambios en el modelo de datos.
 */