import 'package:flutter/material.dart';

class CustomFAbutton extends StatelessWidget {
  final IconData icono;
  final VoidCallback? onPress;

  const CustomFAbutton({
    Key? key,
    required this.icono,
    this.onPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: onPress,
      child: Icon(icono),
    );
  }
}
