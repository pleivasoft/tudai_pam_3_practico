import 'package:app_policia/api/api_service.dart';
import 'package:app_policia/models/escuela.dart';
import 'package:app_policia/services/elecciones_service.dart';
import 'package:app_policia/services/notifications_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class IncidenciaPage extends StatefulWidget {
  IncidenciaPage({Key? key}) : super(key: key);

  @override
  _IncidenciaPageState createState() => _IncidenciaPageState();
}

class _IncidenciaPageState extends State<IncidenciaPage> {
  final _formKey = GlobalKey<FormState>();
  var _ctrlDescripcion = TextEditingController();
  ScrollController textFieldScrollController = ScrollController();
  var empleado;
  var isDatoDisponible = true;
  bool hideError = false;
  bool first = true;

  List<Escuela> escuelas = <Escuela>[];
  var escuela;

  @override
  void initState() {
    super.initState();
    //cargarTurno();
  }

  @override
  Widget build(BuildContext context) {
    var eleccionesService = Provider.of<EleccionesService>(context);
    this.escuelas = eleccionesService.escuelas;
    this.escuela = eleccionesService.escuela;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xff294a70),
          elevation: 0.1,
          title: Text("Registrar incidente"),
        ),
        body: Form(
          key: _formKey,
          autovalidateMode: AutovalidateMode.always,
          child: Stack(children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    DropdownButtonFormField<Escuela>(
                      value: escuela,
                      isExpanded: true,
                      decoration: InputDecoration(
                          labelText: "Escuela",
                          labelStyle: TextStyle(color: Colors.indigo)),
                      icon: const Icon(Icons.arrow_downward),
                      elevation: 16,
                      style: const TextStyle(color: Colors.deepPurple),
                      onChanged: (Escuela? value) {
                        setState(() {
                          escuela = value!;
                        });
                      },
                      items: escuelas
                          .map<DropdownMenuItem<Escuela>>((Escuela value) {
                        return DropdownMenuItem<Escuela>(
                          value: value,
                          child: Text(
                            value.escuela.toString(),
                            style: TextStyle(fontSize: 20),
                          ),
                        );
                      }).toList(),
                    ),
                    Expanded(
                      flex: 2,
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 50.0),
                        child: TextFormField(
                          scrollController: textFieldScrollController,
                          expands: true,
                          maxLines: null,
                          minLines: null,
                          controller: _ctrlDescripcion,
                          style: TextStyle(fontSize: 20),
                          keyboardType: TextInputType.multiline,
                          decoration: const InputDecoration(
                            alignLabelWithHint: true,
                            border: InputBorder.none,
                            fillColor: Colors.white,
                            filled: true,
                            labelText: 'Descripción del incidente',
                            labelStyle: TextStyle(color: Colors.indigo),
                          ),
                          onSaved: (newValue) {
                            setState(() {
                              //paciente.telefono = int.tryParse(newValue.toString());
                            });
                          },
                          onChanged: (value) {
                            /*   textFieldScrollController.jumpTo(
                            textFieldScrollController.position.maxScrollExtent +
                                10); */
                          },
                        ),
                      ),
                    ),
                  ]),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: isDatoDisponible
                        ? Expanded(
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  minimumSize: Size(double.infinity,
                                      48) // put the width and height you want
                                  ),
                              child: Text('Registrar incidente'),
                              onPressed: () async {
                                setState(() {
                                  hideError = true;
                                  isDatoDisponible = false;
                                });

                                if (_ctrlDescripcion.text.isEmpty ||
                                    escuela == null) {
                                  NotificationsService.showSnackbar(
                                      _ctrlDescripcion.text.isEmpty
                                          ? "Describa el incidente"
                                          : escuela ?? "Selccione una escuela");
                                  setState(() {
                                    isDatoDisponible = true;
                                  });
                                  return;
                                }
                                var body = {
                                  "escuelaId": escuela.id,
                                  "descripcion": _ctrlDescripcion.text,
                                };
                                bool estado =
                                    await ApiService().registrarIncidente(body);
                                FocusScope.of(context).unfocus();

                                if (estado) Navigator.pop(context);
                                setState(() {
                                  isDatoDisponible = true;
                                });
                              },
                            ),
                          )
                        : Padding(
                            padding: EdgeInsets.only(bottom: 10.0),
                            child: CircularProgressIndicator()),
                  ),
                ],
              ),
            )
          ]),
        ));
  }

  get textStyle => TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.bold,
        color: Color.fromARGB(153, 98, 60, 235),
      );

  get textStyleTitulo => TextStyle(
        fontSize: 14,
        letterSpacing: 2,
        color: Color.fromARGB(255, 6, 7, 53),
      );

  String? validarTexto(String? value) {
    if (value == null || value.isEmpty) {
      return 'Ingrese un texto';
    }
    return null;
  }

  void limpiarText() {
    _ctrlDescripcion.clear();
  }

  Future<void> cargarTurno() async {
    var eleccionesService = Provider.of<EleccionesService>(context);
    this.escuelas = eleccionesService.escuelas;
    this.escuela = eleccionesService.escuela;
    setState(() {});
  }
}
