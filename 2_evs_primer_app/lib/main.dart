import 'package:flutter/material.dart';
import 'package:primer_app/src/screens/contador_screen.dart';

void main(List<String> args) {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(home: ContadorScreen());
  }
}
