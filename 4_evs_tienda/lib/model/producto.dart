import 'package:flutter/material.dart';

class Producto {
  final int id;
  final String nombre;
  final int precio;
  final ValueNotifier<int> cantidad;
  final IconData icono;

  Producto({
    required this.id,
    required this.nombre,
    required this.precio,
    required this.cantidad,
    required this.icono,
  });

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    return other is Producto && other.id == id;
  }

  @override
  int get hashCode => id.hashCode;

  Producto copyWith({int? id, String? nombre, int? precio, required ValueNotifier<int> cantidad, IconData? icon}) =>
      Producto(id: this.id, nombre: this.nombre, precio: this.precio, cantidad: cantidad, icono: icono);

  Map<String, dynamic> toMap() => {
        "id": id,
        "nombre": nombre,
        "precio": precio,
        "cantidad": cantidad.value,
        "icono": icono.toString(),
      };

  @override
  String toString() {
    int valor = cantidad.value;
    return '{id: $id,nombre: $nombre, precio: $precio, cantidad: $valor}  \n';
  }
}
