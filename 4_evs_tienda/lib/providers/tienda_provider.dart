import 'package:flutter/material.dart';
import 'package:tienda_evs_4/model/producto.dart';

class TiendaProvider extends ChangeNotifier {
  List<Producto> productos = [];

  Set<Producto> productosSeleccionados = {};

  //List<Producto> productosSeleccionados = [];

  int _cantidadEnCarrito = 0;

  TiendaProvider() {
    generarProductos();
  }

  int getCantidadCarrito() {
    return _cantidadEnCarrito;
  }

  int getSubtotal() {
    int total = 0;
    for (var element in productosSeleccionados.toList()) {
      total += (element.precio * element.cantidad.value);
    }
    return total;
  }

  generarProductos() {
    productos.add(Producto(id: 1, nombre: "Hamburguesa", precio: 10, cantidad: ValueNotifier(1), icono: Icons.lunch_dining));
    productos.add(Producto(id: 2, nombre: "Gaseosa", precio: 20, cantidad: ValueNotifier(1), icono: Icons.liquor));
    productos.add(Producto(id: 3, nombre: "Combo H/G", precio: 30, cantidad: ValueNotifier(1), icono: Icons.fastfood));
    productos.add(Producto(id: 4, nombre: "Pizza", precio: 40, cantidad: ValueNotifier(1), icono: Icons.local_pizza));
  }

  void addProductoCarrito(Producto producto) {
    Producto aux = producto.copyWith(cantidad: ValueNotifier(1));
    productosSeleccionados.add(aux);
    _cantidadEnCarrito = productosSeleccionados.length;
    notifyListeners();
  }

  void eliminarProductoSeleccionado(Producto producto) {
    productosSeleccionados.remove(producto);
    _cantidadEnCarrito = productosSeleccionados.length;
    notifyListeners();
  }

  void restarCantidad(Producto producto) {
    producto.cantidad.value = producto.cantidad.value - 1;
    notifyListeners();
  }

  void agregarCantidad(Producto producto) {
    producto.cantidad.value = producto.cantidad.value + 1;
    notifyListeners();
  }
}
