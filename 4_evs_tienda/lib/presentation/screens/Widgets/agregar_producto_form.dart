import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tienda_evs_4/model/producto.dart';
import 'package:tienda_evs_4/providers/productos_provider.dart';

class AgregarProductoForm extends StatefulWidget {
  const AgregarProductoForm({super.key});

  @override
  State<AgregarProductoForm> createState() => _AgregarProductoFormState();
}

class _AgregarProductoFormState extends State<AgregarProductoForm> {
  final _nombreController = TextEditingController();
  final _precioController = TextEditingController();
  final _cantidadController = TextEditingController();
  IconData _iconoSeleccionado = Icons.shopping_cart;

  @override
  void dispose() {
    _nombreController.dispose();
    _precioController.dispose();
    _cantidadController.dispose();
    super.dispose();
  }

  Producto get _producto => Producto(
        id: 0,
        nombre: _nombreController.text,
        precio: int.parse(_precioController.text),
        cantidad: ValueNotifier(int.parse(_cantidadController.text)),
        icono: _iconoSeleccionado,
      );

  void _seleccionarIcono(IconData icono) {
    setState(() {
      _iconoSeleccionado = icono;
    });
  }

  @override
  Widget build(BuildContext context) {
    final provider = context.watch<ProductosProvider>();

    return AlertDialog(
      title: const Text('Agregar Producto'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TextField(
            controller: _nombreController,
            decoration: const InputDecoration(labelText: 'Nombre'),
          ),
          TextField(
            controller: _precioController,
            decoration: const InputDecoration(labelText: 'Precio'),
            keyboardType: TextInputType.number,
          ),
          TextField(
            controller: _cantidadController,
            decoration: const InputDecoration(labelText: 'Cantidad'),
            keyboardType: TextInputType.number,
          ),
          Row(
            children: [
              const Text('Icono:'),
              IconButton(
                icon: Icon(_iconoSeleccionado),
                onPressed: () => showDialog(
                  context: context,
                  builder: (context) => SimpleDialog(
                    title: const Text('Seleccionar Icono'),
                    children: [
                      SimpleDialogOption(
                        onPressed: () => _seleccionarIcono(Icons.shopping_cart),
                        child: const Icon(Icons.shopping_cart),
                      ),
                      SimpleDialogOption(
                        onPressed: () => _seleccionarIcono(Icons.fastfood),
                        child: const Icon(Icons.fastfood),
                      ),
                      // Add more options for other icons if desired
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
      actions: [
        TextButton(
          child: const Text('Cancelar'),
          onPressed: () => Navigator.of(context).pop(),
        ),
        TextButton(
          child: const Text('Agregar'),
          onPressed: () {
            provider.agregarProducto(_producto);
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }
}
