part 'login_body.g.dart';

class LoginBody {
  String username;
  String password;
  String email;
  String tokenFirebase;
  String tokenGoogle;
  String captcha;
  String telefono;

  LoginBody(this.username, this.password, this.email, this.tokenFirebase,
      this.tokenGoogle, this.captcha, this.telefono);

  factory LoginBody.fromJson(Map<String, dynamic> json) =>
      _$LoginBodyFromJson(json);

  Map<String, dynamic> toJson() => _$LoginBodyToJson(this);

  @override
  String toString() {
    return 'LoginBody{usuario: $username, clave: $password, email: $email, tokenFirebase: $tokenFirebase, tokenGooogle: $tokenGoogle, captcha: $captcha, telefono: $telefono}';
  }
}
