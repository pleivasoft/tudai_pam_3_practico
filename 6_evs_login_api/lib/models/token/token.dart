part 'token.g.dart';

class Token {
  late int id;
  late String nombre;
  late String apellido;
  late String roles;
  late String nombreUsuario;
  late String token;
  late String refreshToken;
  late String error = "";

  Token.empty();

  Token(this.id, this.nombre, this.apellido, this.roles, this.nombreUsuario,
      this.token, this.refreshToken);

  factory Token.fromJson(Map<String, dynamic> json) => _$TokenFromJson(json);

  Token.withError(this.error);

  Map<String, dynamic> toJson() => _$TokenToJson(this);

  Token copyWith(
    int id,
    String nombre,
    String apellido,
    String roles,
    String nombreUsuario,
    String token,
    String refreshToken,
  ) =>
      Token(
        id,
        nombre,
        apellido,
        roles,
        nombreUsuario,
        token,
        refreshToken,
      );

  @override
  String toString() {
    return 'Token{nombre: $nombre,apellido: $apellido, token: $token, refreshToken: $refreshToken}';
  }
}
