import 'package:app_policia/models/token/token.dart';
import 'package:app_policia/page/electoral/listado_escuela_page.dart';
import 'package:app_policia/services/elecciones_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:app_policia/screens/screens.dart';
import 'package:app_policia/services/services.dart';

class CheckAuthScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context, listen: false);
    final eleccionesService = Provider.of<EleccionesService>(context, listen: false);

    return Scaffold(
      body: Center(
        child: FutureBuilder(
          future: authService.readToken(),
          builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
            if (!snapshot.hasData)
              return Container(
                  color: Color(0xFF0D47A1),
                  child: Stack(
                    children: [
                      Center(
                        child: Padding(
                            padding: EdgeInsets.all(100),
                            child: Image.asset('assets/logo_policia.png')),
                      ),
                      Align(
                          alignment: Alignment.bottomCenter,
                          child: Padding(
                            padding: const EdgeInsets.all(50.0),
                            child: Image.asset(
                                'assets/logo_ministerio_seguridad.png'),
                          )),
                    ],
                  ));

            if (snapshot.data == '') {
              Future.microtask(() {
                Navigator.pushReplacement(
                    context,
                    PageRouteBuilder(
                        pageBuilder: (_, __, ___) => LoginScreen(),
                        transitionDuration: Duration(seconds: 0)));
              });
            } else {
              WidgetsBinding.instance.addPostFrameCallback((_) async {
                bool respuetas = await cargarDatos(context);
                if (respuetas){
                  if(eleccionesService.escuelas.length>1)
                  Future.microtask(() {
                    Navigator.pushReplacement(
                        context,
                        PageRouteBuilder(
                            pageBuilder: (_, __, ___) => ListadoEscuelasPage(),
                            transitionDuration: Duration(seconds: 0)));
                  });
                  else
                  Future.microtask(() {
                    Navigator.pushReplacement(
                        context,
                        PageRouteBuilder(
                            pageBuilder: (_, __, ___) => HomeScreen(),
                            transitionDuration: Duration(seconds: 0)));
                  });
                }
                else
                  Future.microtask(() {
                    Navigator.pushReplacement(
                        context,
                        PageRouteBuilder(
                            pageBuilder: (_, __, ___) => LoginScreen(),
                            transitionDuration: Duration(seconds: 0)));
                  });
              });
            }

            return Container(
                color: Color(0xFF0D47A1),
                child: Stack(
                  children: [
                    Center(
                      child: Image.asset(
                        'assets/logo_policia.png',
                        width: 230.0,
                        height: 230.0,
                      ),
                    ),
                    Align(
                        alignment: Alignment.bottomCenter,
                        child: Padding(
                          padding: const EdgeInsets.all(50.0),
                          child: Image.asset(
                              'assets/logo_ministerio_seguridad.png'),
                        )),
                  ],
                ));
          },
        ),
      ),
    );
  }

  Future<bool> cargarDatos(context) async {
    try {
      final authService = Provider.of<AuthService>(context, listen: false);
      final eleccionesService = Provider.of<EleccionesService>(context, listen: false);
      var response = await Future.wait([
        authService.loginRefreshForToken(),
        eleccionesService.cargarEscuela()
      ]);
      if ((response[0] as Token).error.isNotEmpty || response[1] == false)
        return false;
      return true;
    } catch (e) {
      return false;
    }
    //var token = await authService.loginRefreshForToken();
    //bool estado = await eleccionesService.cargarEscuela();
  }
}
