import 'dart:io';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class VideoCapturaScreen extends StatefulWidget {
  const VideoCapturaScreen({super.key, required this.camara});
  final CameraDescription camara;
  @override
  VideoCapturaScreenState createState() => VideoCapturaScreenState();
}

class VideoCapturaScreenState extends State<VideoCapturaScreen> {
  late CameraController _controller;
  late Future<void> _initControllerFuture;

  bool _isRecording = false;

  @override
  void initState() {
    super.initState();
    initializeCamera();
  }

  Future<void> initializeCamera() async {
    _controller = CameraController(widget.camara, ResolutionPreset.high);
    //A continuación, inicializa el controlador. Esto devuelve un Future.
    _initControllerFuture = _controller.initialize();
  }

  Future<void> startVideoRecording() async {
    if (!_controller.value.isInitialized) {
      return;
    }

    if (_controller.value.isRecordingVideo) {
      return;
    }

    final status = await Permission.camera.request();
    if (status != PermissionStatus.granted) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('Camera permission denied')),
      );
      return;
    }

    try {
      await _controller.startVideoRecording();
      setState(() {
        _isRecording = true;
      });
    } catch (e) {
      print(e);
    }
  }

  Future<void> stopVideoRecording() async {
    if (!_controller.value.isRecordingVideo) {
      return;
    }

    try {
      final videoFile = await _controller.stopVideoRecording();
      setState(() {
        _isRecording = false;
      });

      String pathFijo = "storage/emulated/0";
      Directory? directory1 = Directory(pathFijo);
      // Crear una nueva ruta para la imagen en el almacenamiento externo
      final newPath = '${directory1.path}/video.mp4';
      File(videoFile.path).copySync(newPath);
      print("Video grabado");
    } catch (e) {
      print(e);
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Grabar Video')),
      body: Stack(
        children: [
          FutureBuilder(
              future: _initControllerFuture,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  // Si el Future  está completo, muestra la vista previa.
                  return CameraPreview(_controller);
                } else {
                  // De lo contrario, muestra un indicador de carga.
                  return const Center(child: CircularProgressIndicator());
                }
              }),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: _isRecording ? stopVideoRecording : startVideoRecording,
        child: Icon(_isRecording ? Icons.stop : Icons.videocam),
      ),
    );
  }
}
