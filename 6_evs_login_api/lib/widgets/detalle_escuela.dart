
import 'package:app_policia/models/escuela.dart';
import 'package:app_policia/services/auth_service.dart';
import 'package:app_policia/services/elecciones_service.dart';
import 'package:flutter/material.dart';

class DetalleEscuela extends StatelessWidget {
  const DetalleEscuela({
    Key? key,
    required this.escuela,
    required this.textStyleTitulo,
    required this.textStyle,
    required this.eleccionesService,
    required this.authoService,
  }) : super(key: key);

  final Escuela? escuela;
  final TextStyle textStyleTitulo;
  final TextStyle textStyle;
  final EleccionesService eleccionesService;
  final AuthService authoService;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 80, left: 20, right: 20),
      color: Color(0xFF3b5999).withOpacity(.85),
      child: this.escuela != null
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                  text: TextSpan(
                    text: escuela!.escuela,
                    style: TextStyle(
                      fontSize: 20,
                      letterSpacing: 2,
                      color: Colors.yellow[700],
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  this.escuela!.dependencia,
                  style: TextStyle(
                    letterSpacing: 1,
                    color: Colors.white,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                RichText(
                  text: TextSpan(
                      text: "Estado: ",
                      style: textStyleTitulo,
                      children: [
                        TextSpan(text: escuela!.estado, style: textStyle)
                      ]),
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RichText(
                      text: TextSpan(
                          text: "Mesas:  ",
                          style: textStyleTitulo,
                          children: [
                            TextSpan(
                              text: escuela!.mesas,
                              style: textStyle,
                            )
                          ]),
                    ),
                    RichText(
                      text: TextSpan(
                          text: "Electores: ",
                          style: textStyleTitulo,
                          children: [
                            TextSpan(
                                text: this.escuela!.electores.toString(),
                                style: textStyle)
                          ]),
                    ),
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RichText(
                      text: TextSpan(
                          text: "Ultima Carga: ",
                          style: textStyleTitulo,
                          children: [
                            TextSpan(
                              text: eleccionesService.escuela!.ultimaCarga
                                  .toString(),
                              style: textStyle,
                            )
                          ]),
                    ),
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RichText(
                      text: TextSpan(
                          text: "Sesión de: ",
                          style: textStyleTitulo,
                          children: [
                            TextSpan(
                              text: authoService.isToken.apellido +
                                  ", " +
                                  authoService.isToken.nombre,
                              style: textStyle,
                            )
                          ]),
                    ),
                  ],
                ),
              ],
            )
          : Center(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    RichText(
                      text: TextSpan(
                        text: "Sin Datos",
                        style: TextStyle(
                          fontSize: 20,
                          letterSpacing: 2,
                          color: Colors.yellow[700],
                        ),
                      ),
                    )
                  ]),
            ),
    );
  }
}
