import 'dart:async';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_media_1/screen/galeria/galeria_screen.dart';
import 'package:flutter_application_media_1/screen/image/capturar_imagen_screen.dart';
import 'package:flutter_application_media_1/screen/video/capturar_video_screen.dart';
import 'package:flutter_application_media_1/screen/video/video_captura_screen.dart';

Future<void> main() async {
  // nos aseguramos que los servicios de flutter esté correctamente inicializado del para que `availableCameras()`
  // se pueda llamar antes de `runApp()`
  WidgetsFlutterBinding.ensureInitialized();

  // Obtener una lista de las cámaras disponibles en el dispositivo.
  final camaras = await availableCameras();

  // Obtenga una cámara específica de la lista de cámaras disponibles.
  final primeraCamara = camaras.first;

  runApp(
    MaterialApp(
      theme: ThemeData.dark(),
      initialRoute: 'VideoCapturaScreen',
      routes: {
        'CapturarImagenScreen': (_) => CapturarImagenScreen(
              camara: primeraCamara, // Pasa la cámara adecuada al widget.
            ),
        'VideoCapturaScreen': (_) => VideoCapturaScreen(
              camara: primeraCamara, // Pasa la cámara adecuada al widget.
            ),
        'CapturarVideoScreen': (context) => const CapturarVideoScreen(),
        'GaleriaScreen': (context) => const GaleriaScreen(),
      },
    ),
  );
}
