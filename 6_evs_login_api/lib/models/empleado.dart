class Empleado {
  int? empleadoId;
  int? dni;
  String? sexo;
  String? apellido;
  String? nombre;
  int? jerarquiaId;
  String? jerarquia;
  int? dependenciaId;
  String? dependencia;
  int? turnoId;

  Empleado(
      {this.empleadoId,
      this.dni,
      this.sexo,
      this.apellido,
      this.nombre,
      this.jerarquiaId,
      this.jerarquia,
      this.dependenciaId,
      this.dependencia,
      this.turnoId});

  Empleado.fromJson(Map<String, dynamic> json) {
    empleadoId = json['empleadoId'];
    dni = json['dni'];
    sexo = json['sexo'];
    apellido = json['apellido'];
    nombre = json['nombre'];
    jerarquiaId = json['jerarquiaId'];
    jerarquia = json['jerarquia'];
    dependenciaId = json['dependenciaId'];
    dependencia = json['dependencia'];
    turnoId = json['turnoId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['empleadoId'] = this.empleadoId;
    data['dni'] = this.dni;
    data['sexo'] = this.sexo;
    data['apellido'] = this.apellido;
    data['nombre'] = this.nombre;
    data['jerarquiaId'] = this.jerarquiaId;
    data['jerarquia'] = this.jerarquia;
    data['dependenciaId'] = this.dependenciaId;
    data['dependencia'] = this.dependencia;
    data['turnoId'] = this.turnoId;
    return data;
  }
}