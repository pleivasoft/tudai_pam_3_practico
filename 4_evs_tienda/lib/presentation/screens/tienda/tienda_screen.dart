import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tienda_evs_4/presentation/screens/Widgets/lista_productos.dart';
import 'package:tienda_evs_4/presentation/screens/tienda/carrito_screen.dart';
import 'package:tienda_evs_4/presentation/screens/tienda/producto_screen.dart';
import 'package:tienda_evs_4/providers/login_provider.dart';
import 'package:tienda_evs_4/providers/tienda_provider.dart';
import 'package:badges/badges.dart' as badges;

class TiendaScreen extends StatelessWidget {
  const TiendaScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Tienda evs"), actions: [
          badges.Badge(
              badgeContent: Consumer<TiendaProvider>(
                builder: (context, value, child) {
                  return Text(
                    value.getCantidadCarrito().toString(),
                    style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                  );
                },
              ),
              position: badges.BadgePosition.topEnd(),
              child: IconButton(
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => const CarritoScreen()));
                  },
                  icon: const Icon(Icons.shopping_cart))),
          IconButton(
              onPressed: () {
                final loginPro = Provider.of<LoginProvider>(context, listen: false);
                loginPro.logout();
              },
              icon: const Icon(Icons.logout)),
          const SizedBox(
            width: 20.0,
          )
        ]),
        body: const ListaProductos(),
        floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.add),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const ProductoScreen(),
                ));
          },
        ));
  }
}
