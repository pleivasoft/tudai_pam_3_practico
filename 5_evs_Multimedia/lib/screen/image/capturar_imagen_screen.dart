import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_media_1/screen/image/ver_imagen_screen.dart';

class CapturarImagenScreen extends StatefulWidget {
  const CapturarImagenScreen({
    super.key,
    required this.camara,
  });

  final CameraDescription camara;

  @override
  CapturarImagenScreenState createState() => CapturarImagenScreenState();
}

class CapturarImagenScreenState extends State<CapturarImagenScreen> {
  late CameraController _controller;
  late Future<void> _initControllerFuture;

  @override
  void initState() {
    super.initState();
    // Para mostrar la salida actual de la cámara, creamos un controlador de cámara.
    _controller = CameraController(
      // Obtenemos la cámara específica de la lista de cámaras disponibles.
      widget.camara,
      // Definir la resolución a utilizar.
      ResolutionPreset.medium,
    );
    //A continuación, inicializa el controlador. Esto devuelve un Future.
    _initControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    // Elimina el controlador cuando se elimina el widget.
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Capturar Imagen')),
      // Debemos esperar hasta que el controlador se inicialice antes de mostrar la vista previa de la cámara.
      // Utilizamos FutureBuilder para mostrar un loading spinner hasta que
      // el controlador haya terminado de inicializarse.
      body: FutureBuilder<void>(
        future: _initControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            // Si el Future  está completo, muestra la vista previa.
            return CameraPreview(_controller);
          } else {
            // De lo contrario, muestra un indicador de carga.
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          // Toma la foto en un bloque try/catch. Si algo sale mal, detecta el error.
          try {
            //Nos aseguramos de que la cámara esté inicializada.
            await _initControllerFuture;

            // Intenta tomar una foto y obtener el archivo `image`
            // donde se guardó.
            final imagen = await _controller.takePicture();

            if (!context.mounted) return;

            // Si la foto fue tomada, muéstrala en una nueva pantalla.
            await Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => VerImagenScreen(
                  imagePath: imagen.path,
                ),
              ),
            );
          } catch (e) {
            print(e);
          }
        },
        child: const Icon(Icons.camera_alt),
      ),
    );
  }
}
