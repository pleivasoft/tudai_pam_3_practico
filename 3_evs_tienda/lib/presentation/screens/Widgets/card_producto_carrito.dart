import 'package:flutter/material.dart';
import 'package:tienda_evs_3/model/producto.dart';
import 'package:tienda_evs_3/presentation/screens/Widgets/botones_menos_mas.dart';
import 'package:tienda_evs_3/providers/tienda_provider.dart';

class CardProductoCarrito extends StatelessWidget {
  const CardProductoCarrito({
    super.key,
    required this.producto,
    required this.tiendaProvider,
  });

  final Producto producto;
  final TiendaProvider tiendaProvider;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.deepPurple[100],
      elevation: 5.0,
      child: ListTile(
          leading: const Icon(Icons.bug_report),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(producto.nombre),
              BotonesMenosMas(
                agregarCantidad: () {
                  tiendaProvider.agregarCantidad(producto);
                },
                restarCantidad: () {
                  tiendaProvider.restarCantidad(producto);
                },
                text: producto.cantidad.value.toString(),
              )
            ],
          ),
          subtitle: RichText(
            text: TextSpan(
              text: 'Precio: ',
              style: DefaultTextStyle.of(context).style,
              children: <TextSpan>[
                TextSpan(text: '\$${producto.precio}', style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
              ],
            ),
          ),
          trailing: IconButton(
              onPressed: () {
                tiendaProvider.eliminarProductoSeleccionado(producto);
              },
              icon: const Icon(
                Icons.delete,
                color: Colors.red,
              ))),
    );
  }
}
