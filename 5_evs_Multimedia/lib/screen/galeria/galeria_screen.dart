import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:permission_handler/permission_handler.dart';
import 'dart:io';

class GaleriaScreen extends StatefulWidget {
  const GaleriaScreen({super.key});

  @override
  GaleriaScreenState createState() => GaleriaScreenState();
}

class GaleriaScreenState extends State<GaleriaScreen> {
  File? _media;

  Future<void> _abrirGaleria() async {
    final picker = ImagePicker();
    final archivoElejido = await picker.pickImage(source: ImageSource.gallery);

    if (archivoElejido != null) {
      setState(() {
        _media = File(archivoElejido.path);
      });
    }
  }

  Future<void> _grabarEnGaleria() async {
    if (_media != null) {
      PermissionStatus status = await Permission.storage.request();
      if (!status.isGranted) {
        status = await Permission.manageExternalStorage.request();
      }
      if (status.isGranted) {
        final result = await ImageGallerySaver.saveFile(_media!.path);

        // ignore: use_build_context_synchronously
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(result['isSuccess'] ? 'Grabado con exito!' : 'Error al grabar.')),
        );
      } else {
        // ignore: use_build_context_synchronously
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Permiso denegado')),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Elegir y grabar imagen')),
      body: Center(
        child: _media == null ? const Text('Elija una imagen de la galeria.') : Image.file(_media!),
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            onPressed: _abrirGaleria,
            tooltip: 'Abrir galeria',
            child: const Icon(Icons.photo_library),
          ),
          const SizedBox(height: 10),
          FloatingActionButton(
            onPressed: _grabarEnGaleria,
            tooltip: 'Grabar en galeria',
            child: const Icon(Icons.save),
          ),
        ],
      ),
    );
  }
}
