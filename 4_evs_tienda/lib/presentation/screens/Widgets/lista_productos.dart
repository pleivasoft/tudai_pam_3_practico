import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tienda_evs_4/providers/tienda_provider.dart';

class ListaProductos extends StatelessWidget {
  const ListaProductos({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final tiendaProvider = context.watch<TiendaProvider>();
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          children: [
            Expanded(
                child: ListView.builder(
              itemCount: tiendaProvider.productos.length,
              itemBuilder: (context, index) {
                final producto = tiendaProvider.productos[index];
                return Card(
                  color: Colors.blueGrey.shade200,
                  elevation: 5.0,
                  child: ListTile(
                    leading: Icon(
                      producto.icono,
                      color: Colors.white,
                    ),
                    title: Text(producto.nombre),
                    subtitle: RichText(
                      text: TextSpan(
                        text: 'Precio: ',
                        style: DefaultTextStyle.of(context).style,
                        children: <TextSpan>[
                          TextSpan(text: '\$${producto.precio}', style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                        ],
                      ),
                    ),
                    trailing: IconButton(
                      onPressed: () {
                        tiendaProvider.addProductoCarrito(producto);
                      },
                      icon: const Icon(
                        Icons.add_shopping_cart_outlined,
                        color: Colors.white,
                      ),
                    ),
                  ),
                );
              },
            ))
          ],
        ),
      ),
    );
  }
}
