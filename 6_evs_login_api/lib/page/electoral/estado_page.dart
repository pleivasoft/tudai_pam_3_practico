import 'package:app_policia/api/api_service_action.dart';
import 'package:app_policia/models/EstadoEscuela.dart';
import 'package:app_policia/models/response.dart';
import 'package:app_policia/services/elecciones_service.dart';
import 'package:app_policia/services/notifications_service.dart';
import 'package:app_policia/utils/dialog_estado.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EstadoEleccionesPage extends StatefulWidget {
  EstadoEleccionesPage({Key? key}) : super(key: key);

  @override
  _EstadoEleccionesPageState createState() => _EstadoEleccionesPageState();
}

class _EstadoEleccionesPageState extends State<EstadoEleccionesPage> {
  var isDatoDisponible = true;
  bool hideError = false;
  late EleccionesService eleccionesService;
  List<EstadoEscuela> estados = <EstadoEscuela>[];

  bool registrando = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    eleccionesService = Provider.of<EleccionesService>(context, listen: false);
    cargarEstadoEscuela();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xff294a70),
          elevation: 0.1,
          title: Text("Estado eleccion"),
        ),
        body: Column(
          children: [
            ListView.separated(
              shrinkWrap: true,
              itemCount: this.estados.length,
              separatorBuilder: (BuildContext context, int index) {
                return Divider();
              },
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  title: Text(
                    this.estados[index].descripcion.toString(),
                  ),
                  subtitle: Text(
                    this.estados[index].fecha != null
                        ? this.estados[index].strFecha
                        : "",
                  ),
                  trailing: Switch(
                    value: this.estados[index].estado,
                    onChanged: (value) {
                      if (value) confirmaEstado(value, index);
                    },
                    activeTrackColor: Color.fromARGB(255, 89, 119, 255),
                    activeColor: Color.fromARGB(255, 4, 21, 172),
                  ),
                );
              },
            ),
          ],
        ));
  }

  get textStyle => TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.bold,
        color: Color.fromARGB(153, 98, 60, 235),
      );

  get textStyleTitulo => TextStyle(
        fontSize: 14,
        letterSpacing: 2,
        color: Color.fromARGB(255, 6, 7, 53),
      );

  Future<void> cargarEstadoEscuela() async {
    this.estados =
        await ApiAction().obtenerEstados(eleccionesService.escuela!.id);
    setState(() {});
  }

  Future<void> confirmaEstado(bool value, int index) async {
    int estadoId = this.estados[index].escuelaEstadoId as int;
    try {
      Respuesta result =
          await DialogEstado.showAlertDialog(context, estadoId, registrando);
      setState(() {
        if (result.estado) {
          this.estados[index].estado = value;
          this.estados[index].fecha = DateTime.tryParse(result.data!["fecha"]);
        } 
        else {
          NotificationsService.showSnackbarError(result.mensaje);
        }
      });
    } catch (e) {}
  }
}
