class Respuesta {
  late bool estado;
  late String mensaje;
  late Map<String, dynamic>? data;

  Respuesta({required this.estado, required this.mensaje, this.data});

  Respuesta.fromJson(Map<String, dynamic> json) {
    estado = json['estado'];
    mensaje = json['mensaje'];
    data = json['data'] != null ? json['data'] : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['estado'] = this.estado;
    data['mensaje'] = this.mensaje;
    if (this.data != null) {
      data['data'] = this.data.toString();
    }
    return data;
  }
}
