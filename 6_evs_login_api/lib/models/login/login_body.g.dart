part of 'login_body.dart';

LoginBody _$LoginBodyFromJson(Map<String, dynamic> json) {
  return LoginBody(
    json['usuario'] as String,
    json['clave'] as String,
    json['email'] as String,
    json['tokenFirebase'] as String,
    json['tokenGooogle'] as String,
    json['telefono'] as String,
    json['telefono'] as String,
  );
}

Map<String, dynamic> _$LoginBodyToJson(LoginBody instance) => <String, dynamic>{
      'username': instance.username,
      'password': instance.password,
      'email': instance.email,
      'tokenFirebase': instance.tokenFirebase,
      'tokenGooogle': instance.tokenGoogle,
      'captcha': instance.captcha,
      'telefono': instance.telefono,
    };
