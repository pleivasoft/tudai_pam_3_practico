import 'package:app_policia/api/api_service.dart';
import 'package:app_policia/models/escuela.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class EleccionesService extends ChangeNotifier {
  final storage = new FlutterSecureStorage();
  late Escuela? escuela;
  late List<Escuela> escuelas = <Escuela>[];

  int cantMayor = 0;
  late bool isLoading;

  Future<bool> cargarEscuela() async {
    this.isLoading = true;
    bool tieneVarias = false;
    this.escuelas = await ApiService().getEscuela();
    if (this.escuelas.isNotEmpty) {
      if (this.escuelas.length == 1) {
        this.escuela = this.escuelas.first;
        tieneVarias = true;
      } else
        tieneVarias = true;
      this.isLoading = false;
      notifyListeners();
    }
    return tieneVarias;
  }

  void cargarCantidad(int hora, int cantidad) async {
    try {
      this.escuela!.carga.firstWhere((element) => element.id == hora).cantidad =
          cantidad;
      this.escuela!.ultimaCarga = cantidad;
    } catch (e) {
      if (hora == 99) escuela!.ultimaCarga = cantidad;
    }
    notifyListeners();
  }

  void limpiar() async {
    this.escuela = null;
    this.escuelas = <Escuela>[];
    notifyListeners();
  }

  Future<void> refreshEscuelas() async {
    var escuelasAux = await ApiService().getEscuela();
    if (escuelasAux.isNotEmpty) {
      this.escuelas = escuelasAux;
      if (this.escuela != null) {
        this.escuela = this
            .escuelas
            .firstWhere((element) => element.id == this.escuela!.id);
      }
    }
    notifyListeners();
  }

/* 
  Future saveOrCreateProduct(Product product) async {
    isSaving = true;
    notifyListeners();

    if (product.id == null) {
      // Es necesario crear
      await this.createProduct(product);
    } else {
      // Actualizar
      await this.updateProduct(product);
    }

    isSaving = false;
    notifyListeners();
  }

  Future<String> updateProduct(Product product) async {
    final url = Uri.https(_baseUrl, 'products/${product.id}.json',
        {'auth': await storage.read(key: 'token') ?? ''});

    final resp = await http.put(url, body: product.toJson());
    final decodedData = resp.body;

    //TODO: Actualizar el listado de productos
    final index =
        this.products.indexWhere((element) => element.id == product.id);
    this.products[index] = product;

    return product.id!;
  }

  Future<String> createProduct(Product product) async {
    final url = Uri.https(_baseUrl, 'products.json',
        {'auth': await storage.read(key: 'token') ?? ''});

    final resp = await http.post(url, body: product.toJson());
    final decodedData = json.decode(resp.body);

    product.id = decodedData['name'];

    this.products.add(product);

    return product.id!;
  }

  void updateSelectedProductImage(String path) {
    this.selectedProduct.picture = path;
    this.newPictureFile = File.fromUri(Uri(path: path));

    notifyListeners();
  }

  Future<String?> uploadImage() async {
    if (this.newPictureFile == null) return null;

    this.isSaving = true;
    notifyListeners();

    final url = Uri.parse(
        'https://api.cloudinary.com/v1_1/dx0pryfzn/image/upload?upload_preset=autwc6pa');

    final imageUploadRequest = http.MultipartRequest('POST', url);

    final file =
        await http.MultipartFile.fromPath('file', newPictureFile!.path);

    imageUploadRequest.files.add(file);

    final streamResponse = await imageUploadRequest.send();
    final resp = await http.Response.fromStream(streamResponse);

    if (resp.statusCode != 200 && resp.statusCode != 201) {
      print('algo salio mal');
      print(resp.body);
      return null;
    }

    this.newPictureFile = null;

    final decodedData = json.decode(resp.body);
    return decodedData['secure_url'];
  } */
}
