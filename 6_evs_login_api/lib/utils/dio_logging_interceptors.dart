import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'package:app_policia/api/api_auth_action.dart';
import 'package:app_policia/models/refreshtoken/refresh_token_body.dart';
import 'package:app_policia/models/token/token.dart';

class DioLoggingInterceptors extends InterceptorsWrapper {
  static StreamController<int> _responseCodeStream =
      new StreamController.broadcast();
// Create storage
  final storage = new FlutterSecureStorage();
  static Stream<int> get errorResponseCode => _responseCodeStream.stream;

  late final Dio _dio;
  DioLoggingInterceptors(this._dio);

  @override
  Future onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    //_prefs = await SharedPreferences.getInstance();
    print(
        "--> ${options.method != null ? options.method.toUpperCase() : 'METHOD'} ${"" + (options.baseUrl) + (options.path)}");

    if (options.headers.containsKey('requirestoken')) {
      options.headers.remove('requirestoken');
      String? accessToken = await storage.read(key: 'token') ?? "";

      //_sharedPreferencesManager.getString(SharedPreferencesManager.keyAccessToken);
      options.headers.addAll({'Authorization': 'Bearer $accessToken'});
    }
    print("Headers:");
    options.headers.forEach((k, v) => print('$k: $v'));

    if (options.queryParameters != null) {
      print("queryParameters:");
      options.queryParameters.forEach((k, v) => print('$k: $v'));
    }
    if (options.data != null) {
      print("Body: ${options.data}");
    }
    print(
        "--> END ${options.method != null ? options.method.toUpperCase() : 'METHOD'}");
    return handler.next(options);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    print(
        "<-- ${response.statusCode} ${(response.requestOptions != null ? (response.requestOptions.baseUrl + response.requestOptions.path) : 'URL')}");
    print("Headers:");
    response.headers.forEach((k, v) => print('$k: $v'));
    print("Response: ${response.data}");
    print("<-- END HTTP");

    super.onResponse(response, handler);
  }

  @override
  Future onError(DioError dioError, ErrorInterceptorHandler handler) async {
    print(
        "<-- ${dioError.message} ${(dioError.response?.requestOptions != null ? (dioError.response!.requestOptions.baseUrl + dioError.response!.requestOptions.path) : 'URL')}");

    if (dioError.type == DioErrorType.connectTimeout) {
      //throw Exception("Connection  Timeout Exception");
      _dio.unlock();
      _dio.unlock();
      super.onError(dioError, handler);
      return;
    }
    print(
        "${dioError.response != null ? dioError.response!.data : 'Unknown Error'}");
    print("<-- End error");
    try {
      int responseCode = dioError.response!.statusCode!;
      String? oldAccessToken = await storage.read(key: 'token') ?? "";
      //_sharedPreferencesManager.getString(SharedPreferencesManager.keyAccessToken);
      if (oldAccessToken != null &&
          oldAccessToken != "" &&
          responseCode == 401 &&
          !dioError.requestOptions.path.contains("refresh-token")) {
        _dio.interceptors.requestLock.lock();
        //_dio.interceptors.responseLock.lock();

        String? refreshToken = await storage.read(key: 'tokenrefresh') ?? "";
        //_sharedPreferencesManager.getString(SharedPreferencesManager.keyRefreshToken);

        RefreshTokenBody refreshTokenBody =
            RefreshTokenBody('refresh_token', refreshToken, "");
        ApiAuthAction apiAuthRepository = ApiAuthAction();
        Token token = await apiAuthRepository.postRefreshAuth(refreshTokenBody);

        //validar error en response request
        String newAccessToken = token.token;
        String newRefreshToken = token.refreshToken;

        await storage.write(key: "token", value: newAccessToken);
        await storage.write(key: "tokenrefresh", value: newRefreshToken);

        //await _sharedPreferencesManager.putString(SharedPreferencesManager.keyAccessToken, newAccessToken);
        //await _sharedPreferencesManager.putString(SharedPreferencesManager.keyRefreshToken, newRefreshToken);

        RequestOptions options = dioError.response!.requestOptions;
        options.headers.addAll({'requiresToken': true});
        /* _dio.interceptors.requestLock.unlock();
      _dio.interceptors.responseLock.unlock(); */
        return _dio.request(options.path); //, options: options
      } else {
        _responseCodeStream.sink.add(responseCode);
        super.onError(dioError, handler);
        //print("Error de token ");
        // LoginState(false).logout();
      }
    } catch (e) {
      super.onError(dioError, handler);
      return;
    }
  }

  static closeStream() {
    _responseCodeStream.close();
  }
}
