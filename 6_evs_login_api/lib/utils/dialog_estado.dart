import 'package:app_policia/api/api_service_action.dart';
import 'package:app_policia/models/response.dart';
import 'package:app_policia/services/auth_service.dart';
import 'package:app_policia/services/elecciones_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DialogEstado {
  static showAlertDialog(
      BuildContext context, int estadoId, bool registrando) async {
    Widget cancelButton = TextButton(
      child: Text("Cancelar"),
      onPressed: () {
        Navigator.pop(
            context, Respuesta(estado: false, mensaje: "Cancela action"));
      },
    );
    Widget continueButton = StatefulBuilder(
      builder: (context, setState) {
        return TextButton(
          child: registrando
              ? SizedBox(
                  child: CircularProgressIndicator(),
                  height: 20,
                  width: 20,
                )
              : Text(estadoId == 8 ? "Finalizar Servicio" : "Confirmar"),
          onPressed: registrando
              ? null
              : () async {
                  registrando = true;
                  setState(() {});
                  await registarEstado(context, estadoId);
                  registrando = false;
                },
        );
      },
    );

    AlertDialog alert = AlertDialog(
      title: Text("Cambio de estado"),
      content: StatefulBuilder(
        builder: (context, setState) {
          return Text(
              "Una vez confirmado no podra revertir el estado.\nDesea Confirmar?");
        },
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    return await showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  static Future<void> registarEstado(BuildContext context, int estadoId) async {
    EleccionesService eleccionesService =
        Provider.of<EleccionesService>(context, listen: false);
    AuthService authService = Provider.of<AuthService>(context, listen: false);
    var token = authService.isToken;
    var body = {
      "dni": int.parse(token.nombreUsuario),
      "estadoId": estadoId,
      "escuelaId": eleccionesService.escuela!.id
    };
    var result = await ApiAction().registrarEstado(body);
    Navigator.pop(context, result);
  }
}
