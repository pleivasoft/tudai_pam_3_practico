import 'package:flutter/material.dart';
import 'package:tienda_evs_4/database/productos_db_helper.dart';
import 'package:tienda_evs_4/model/producto.dart';

class ProductosProvider extends ChangeNotifier {
  List<Producto> _productos = [];
  List<Producto> get productos => _productos;

  void cargarProductos() async {
    _productos = await ProductosDBHelper.getProductos();
    notifyListeners();
  }

  void agregarProducto(Producto producto) async {
    await ProductosDBHelper.insertProducto(producto);
    _productos.add(producto);
    notifyListeners();
  }

  void actualizarProducto(Producto producto) async {
    await ProductosDBHelper.updateProducto(producto);
    _productos[_productos.indexWhere((p) => p.id == producto.id)] = producto;
    notifyListeners();
  }

  void eliminarProducto(int id) async {
    await ProductosDBHelper.deleteProducto(id);
    _productos.removeWhere((p) => p.id == id);
    notifyListeners();
  }
}
