import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tienda_evs_3/model/producto.dart';
import 'package:tienda_evs_3/presentation/screens/Widgets/card_producto_carrito.dart';
import 'package:tienda_evs_3/providers/tienda_provider.dart';

class CarritoScreen extends StatelessWidget {
  const CarritoScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Mi carrito"),
        centerTitle: true,
      ),
      body: ListaProductoSelecciondos(),
    );
  }
}

class ListaProductoSelecciondos extends StatelessWidget {
  const ListaProductoSelecciondos({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final tiendaProvider = context.watch<TiendaProvider>();
    return SafeArea(
        child: Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 10,
      ),
      child: Column(
        children: [
          Expanded(child: Consumer<TiendaProvider>(
            builder: (context, provider, child) {
              if (provider.productosSeleccionados.isEmpty) {
                return const Center(
                  child: Text("Carrito vacio"),
                );
              } else {
                return ListView.builder(
                  itemCount: provider.productosSeleccionados.length,
                  itemBuilder: (context, index) {
                    Producto producto = provider.productosSeleccionados.toList()[index];
                    return CardProductoCarrito(producto: producto, tiendaProvider: provider);
                  },
                );
              }
            },
          ))
        ],
      ),
    ));
  }
}
