import 'package:flutter/material.dart';
import 'package:tienda_evs_4/presentation/screens/Widgets/login_form.dart';

// Pantalla de inicio de sesión
class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Inicio de Sesión')),
      body: const Center(
        child: LoginForm(),
      ),
    );
  }
}
