import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tienda_evs_3/presentation/screens/tienda/tienda_screen.dart';
import 'package:tienda_evs_3/providers/tienda_provider.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => TiendaProvider(),
        )
      ],
      child: const MaterialApp(
        title: 'Tienda App',
        debugShowCheckedModeBanner: false,
        home: TiendaScreen(),
      ),
    );
  }
}
