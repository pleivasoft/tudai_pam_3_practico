import 'dart:convert';

import 'package:app_policia/api/api_auth_action.dart';
import 'package:app_policia/models/login/login_body.dart';
import 'package:app_policia/models/refreshtoken/refresh_token_body.dart';
import 'package:app_policia/models/token/token.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'package:http/http.dart' as http;

class AuthService extends ChangeNotifier {
  final String _baseUrl = 'identitytoolkit.googleapis.com';
  final String _firebaseToken = 'AIzaSyBcytoCbDUARrX8eHpcR-Bdrdq0yUmSjf8';

  final storage = new FlutterSecureStorage();
  final ApiAuthAction apiAuthRepository = ApiAuthAction();

  late Token token = Token.empty();

  Token get isToken => token;

  Future<Token> loginDevice(String dni, String password, int telefono) async {
    //Read value
    String tokenNotificacion =
        await storage.read(key: 'tokenNotificacion') ?? "";
    LoginBody loginBody = LoginBody(
        dni,
        password,
        "device@ppc.com",
        "strTokenFirebase.toString()",
        "Pruebaaaa",
        tokenNotificacion,
        telefono.toString());
    token = await apiAuthRepository.postLoginUser(loginBody);
    this.grabarDataToken(token);
    return token;
  }

  Future<Token?> loginRefreshForToken() async {
    final accessToken = await storage.read(key: 'accessToken');
    final refreshToken = await storage.read(key: 'refreshToken');
    if (refreshToken == null || accessToken == null) return null;

    RefreshTokenBody refreshTokenBody =
        RefreshTokenBody(accessToken, refreshToken, "sa");
    token = await apiAuthRepository.postRefreshAuth(refreshTokenBody);
    this.grabarDataToken(token);
    return token;
  }

  void grabarDataToken(Token token) async {
    final storage = new FlutterSecureStorage();
    if (token.error.isEmpty) {
      await storage.write(key: "accessToken", value: token.token);
      await storage.write(key: "refreshToken", value: token.refreshToken);
      await storage.write(key: "username", value: token.nombreUsuario);
      await storage.write(
          key: "apellidoNombre", value: token.apellido + " " + token.nombre);
    }
  }

  // Si retornamos algo, es un error, si no, todo bien!
  Future<String?> createUser(String email, String password) async {
    final Map<String, dynamic> authData = {
      'email': email,
      'password': password,
      'returnSecureToken': true
    };

    final url =
        Uri.https(_baseUrl, '/v1/accounts:signUp', {'key': _firebaseToken});

    final resp = await http.post(url, body: json.encode(authData));
    final Map<String, dynamic> decodedResp = json.decode(resp.body);

    if (decodedResp.containsKey('idToken')) {
      // Token hay que guardarlo en un lugar seguro
      await storage.write(key: 'token', value: decodedResp['idToken']);
      // decodedResp['idToken'];
      return null;
    } else {
      return decodedResp['error']['message'];
    }
  }

  Future<String?> login(String email, String password) async {
    final Map<String, dynamic> authData = {
      'email': email,
      'password': password,
      'returnSecureToken': true
    };

    final url = Uri.https(
        _baseUrl, '/v1/accounts:signInWithPassword', {'key': _firebaseToken});

    final resp = await http.post(url, body: json.encode(authData));
    final Map<String, dynamic> decodedResp = json.decode(resp.body);

    if (decodedResp.containsKey('idToken')) {
      // Token hay que guardarlo en un lugar seguro
      // decodedResp['idToken'];
      await storage.write(key: 'token', value: decodedResp['idToken']);
      return null;
    } else {
      return decodedResp['error']['message'];
    }
  }

  Future logout() async {
    await storage.deleteAll();
    return;
  }

  Future<String> readToken() async {
    return await storage.read(key: 'accessToken') ?? '';
  }
}
