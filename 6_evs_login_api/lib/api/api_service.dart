import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'package:app_policia/api/environment.dart';
import 'package:app_policia/main.dart';
import 'package:app_policia/models/EstadoEscuela.dart';
import 'package:app_policia/models/turno.dart';
import 'package:app_policia/models/empleado.dart';
import 'package:app_policia/models/escuela.dart';
import 'package:app_policia/models/refreshtoken/refresh_token_body.dart';
import 'package:app_policia/models/response.dart';
import 'package:app_policia/services/notifications_service.dart';

class ApiService {
  var _dio = Dio();

  var tokenDio = Dio();
  final _storage = const FlutterSecureStorage();
  String? csrfToken;

  ApiService() {
    (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };

    _dio.options.baseUrl = Environment.apiUrl;
    //_dio.options.receiveDataWhenStatusError = true;
    _dio.options.connectTimeout = 100 * 100;
    _dio.options.receiveTimeout = 10 * 1000;
    tokenDio.options = _dio.options;

    _dio.interceptors.add(QueuedInterceptorsWrapper(
      onRequest: (options, handler) async {
        print('send request：path:${options.path}，baseURL:${options.baseUrl}');
        if (!options.path.contains('auth')) {
          String accessToken = await _storage.read(key: 'accessToken') ?? "";
          options.headers.addAll({'Authorization': 'Bearer $accessToken'});
          ImprimirRequest(options);
        }
        // ImprimirRequest(options);
        return handler.next(options);
      },
      onError: (error, handler) async {
        //print(error);
        // Assume 401 stands for token expired
        if (error.response?.statusCode == 401 &&
            !error.response!.realUri.toString().contains("refresh")) {
          /* // If the token has been updated, repeat directly.
          if (csrfToken != options.headers['csrfToken']) {
            options.headers['csrfToken'] = csrfToken;
            //repeat
            _dio.fetch(options).then(
              (r) => handler.resolve(r),
              onError: (e) {
                handler.reject(e);
              },
            );
            return;
          } */

          if (await getRefreshToken()) {
            return handler.resolve(await _retry(error.requestOptions));
          } else {
            navigatorKey.currentState!.pushNamedAndRemoveUntil(
                "login", (Route<dynamic> route) => false);
          }

          /* var response = await tokenDio.post(
            Environment.apiUrl + '/auth/refresh-token',
            data: refreshTokenBody.toJson(),
          );

          await _dio
              .post(
            '/auth/refresh-token',
            data: refreshTokenBody.toJson(),
          )
              .then((d) {
            //update csrfToken
            accessToken = d.data['data']['token'];
            options.headers.addAll({'Authorization': 'Bearer $accessToken'});
            //options.headers['csrfToken'] = csrfToken = d.data['data']['token'];
          }).then((e) {
            //repeat
            _dio.fetch(options).then(
              (r) => handler.resolve(r),
              onError: (e) {
                handler.reject(e);
              },
            );
          }); */
        } else {
          navigatorKey.currentState!.pushReplacementNamed("login");
        }
        return handler.next(error);
      },
      onResponse: (response, handler) {
        print(
            "<-- ${response.statusCode} ${(response.requestOptions.baseUrl + response.requestOptions.path)}");
        print("Headers:");
        response.headers.forEach((k, v) => print('$k: $v'));
        print("Response: ${response.data}");
        print("<-- END HTTP");

        return handler.next(response);
      },
    ));
  }

  void ImprimirRequest(RequestOptions options) {
    print("Headers:");
    options.headers.forEach((k, v) => print('$k: $v'));
    if (options.queryParameters != null) {
      print("queryParameters:");
      options.queryParameters.forEach((k, v) => print('$k: $v'));
    }
    if (options.data != null) {
      print("Body: ${options.data}");
    }
  }

  Future<bool> getRefreshToken() async {
    final refreshToken = await _storage.read(key: 'refreshToken');
    final accessToken = await _storage.read(key: 'accessToken');

    RefreshTokenBody refreshTokenBody =
        RefreshTokenBody(accessToken, refreshToken, "sa");
    try {
      var response = await tokenDio.post('/auth/refresh-token',
          data: refreshTokenBody.toJson());

      if (response.statusCode == 200) {
        await _storage.write(key: "accessToken", value: response.data["token"]);
        await _storage.write(
            key: "refreshToken", value: response.data["refreshToken"]);
        return true;
      } else {
        // refresh token is wrong
        _storage.deleteAll();
        return false;
      }
    } on DioError catch (e) {
      return false;
    }
  }

  Future<Response<dynamic>> _retry(RequestOptions requestOptions) async {
    String accessToken = await _storage.read(key: 'accessToken') ?? "";
    final options = Options(
      method: requestOptions.method,
      headers: requestOptions.headers,
    );
    return _dio.request<dynamic>(requestOptions.path,
        data: requestOptions.data,
        queryParameters: requestOptions.queryParameters,
        options: options);
  }

  //Future<Escuela?> getEscuela() async {
  Future<List<Escuela>> getEscuela() async {
    List<Escuela> lista = <Escuela>[];
    try {
      var responde = await _dio.get('/escuela/device');
      lista = (responde.data as List)
          .map((e) => Escuela.fromJson(e as Map<String, dynamic>))
          .toList();
      return lista;
      //var escuela = Escuela.fromJson(responde.data);
      //return escuela;
    } catch (e) {
      var err = e as DioError;
      if(err.response!.statusCode!=401)
        NotificationsService.showSnackbar(err.message);
      print(e.response!.data);
      //return null;
      return lista;
    }
  }

  Future<bool> cargarNovedad(body) async {
    try {
      var responde = await _dio.post('/escuela/electores/novedad', data: body);
      return responde.data;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

  Future<Empleado?> buscarEmpleado(dni) async {
    try {
      var responde = await _dio.get('/empleado/' + dni);
      var empleado = Empleado.fromJson(responde.data);
      return empleado;
    } catch (e) {
      var err = e as DioError;
      NotificationsService.showSnackbar(e.response!.data);
      return null;
    }
  }

  Future<List<Empleado>> obtenerPersonalEscuelaPorDni(body) async {
    List<Empleado> lista = <Empleado>[];
    try {
      var responde = await _dio.post('/escuela/personalpordni', data:body);
      if(responde.data.isEmpty)
        return lista;
        lista = (responde.data as List)
          .map((e) => Empleado.fromJson(e as Map<String, dynamic>))
          .toList();
      return lista;
      //var empleado = Empleado.fromJson(responde.data);
      //return empleado;
    } catch (e) {
      var err = e as DioError;
      NotificationsService.showSnackbar(e.response!.data);
      return lista;
    }
  }

  Future<List<Turno>> obtenerTurnos() async {
    List<Turno> lista = <Turno>[];
    try {
      var response = await _dio.get('/escuela/turnos');
      lista = (response.data as List)
          .map((e) => Turno.fromJson(e as Map<String, dynamic>))
          .toList();
      return lista;
    } catch (e) {
      var err = e as DioError;
      NotificationsService.showSnackbar(e.response!.data);
      print(e.response!.data);
      return lista;
    }
  }

  Future<bool> registrarAsistencia(body) async {
    try {
      var responde = await _dio.post('/escuela/asistencia', data: body);
      return responde.data;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

  /* await Future.wait([
    dio.get('/test?tag=1').then(_onResult),
    dio.get('/test?tag=2').then(_onResult),
    dio.get('/test?tag=3').then(_onResult)
  ]);
   */

  onError() {
    print("error");
  }

  Future<bool> registrarIncidente(body) async {
    try {
      var responde = await _dio.post('/escuela/incidente', data: body);
      return responde.data;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

  Future<List<EstadoEscuela>> obtenerEstados(escuelaId) async {
    try {
      var response = await _dio.get('/escuela/estados/' + escuelaId.toString());
      var lista = (response.data as List)
          .map((e) => EstadoEscuela.fromJson(e as Map<String, dynamic>))
          .toList();

      return lista;
    } catch (e) {
      var err = e as DioError;
      NotificationsService.showSnackbar(e.response!.data);
      print(e.response!.data);
      return <EstadoEscuela>[];
    }
  }

  Future<Respuesta> registrarEstado(body) async {
    try {
      var response = await _dio.post('/escuela/estado', data: body);
      var result = Respuesta.fromJson(response.data);
      return result;
    } catch (e) {
      print(e.toString());

      return Respuesta(
          estado: false,
          mensaje: e.toString(),
          data: new Map<String, dynamic>());
    }
  }
}

/*

import 'dart:async';
import 'package:dio/dio.dart';

void main() async {
  var dio = Dio();
  //dio instance to request token
  var tokenDio = Dio();
  String? csrfToken;
  dio.options.baseUrl = 'http://www.dtworkroom.com/doris/1/2.0.0/';
  tokenDio.options = dio.options;
  dio.interceptors.add(QueuedInterceptorsWrapper(
    onRequest: (options, handler) {
      print('send request：path:${options.path}，baseURL:${options.baseUrl}');
      if (csrfToken == null) {
        print('no token，request token firstly...');
        tokenDio.get('/token').then((d) {
          options.headers['csrfToken'] = csrfToken = d.data['data']['token'];
          print('request token succeed, value: ' + d.data['data']['token']);
          print(
              'continue to perform request：path:${options.path}，baseURL:${options.path}');
          handler.next(options);
        }).catchError((error, stackTrace) {
          handler.reject(error, true);
        });
      } else {
        options.headers['csrfToken'] = csrfToken;
        return handler.next(options);
      }
    },
    onError: (error, handler) {
      //print(error);
      // Assume 401 stands for token expired
      if (error.response?.statusCode == 401) {
        var options = error.response!.requestOptions;
        // If the token has been updated, repeat directly.
        if (csrfToken != options.headers['csrfToken']) {
          options.headers['csrfToken'] = csrfToken;
          //repeat
          dio.fetch(options).then(
            (r) => handler.resolve(r),
            onError: (e) {
              handler.reject(e);
            },
          );
          return;
        }
        tokenDio.get('/token').then((d) {
          //update csrfToken
          options.headers['csrfToken'] = csrfToken = d.data['data']['token'];
        }).then((e) {
          //repeat
          dio.fetch(options).then(
            (r) => handler.resolve(r),
            onError: (e) {
              handler.reject(e);
            },
          );
        });
        return;
      }
      return handler.next(error);
    },
  ));

  FutureOr<void> _onResult(d) {
    print('request ok!');
  }

  await Future.wait([
    dio.get('/test?tag=1').then(_onResult),
    dio.get('/test?tag=2').then(_onResult),
    dio.get('/test?tag=3').then(_onResult)
  ]);
}
*/