part of 'token.dart';

Token _$TokenFromJson(Map<String, dynamic> json) {
  return Token(
    json['id'] as int,
    json['nombre'] as String,
    json['apellido'] as String,
    json['roles'] as String,
    json['nombreUsuario'] as String,
    json['token'] as String,
    json['refreshToken'] as String,
  );
}

Map<String, dynamic> _$TokenToJson(Token instance) => <String, dynamic>{
      'id': instance.id,
      'nombre': instance.nombre,
      'apellido': instance.apellido,
      'roles': instance.roles,
      'nombreUsuario': instance.nombreUsuario,
      'token': instance.token,
      'refreshToken': instance.refreshToken,
    };
