import 'dart:io';

class Environment {
  //static String ipserver = "192.168.100.3:5000";
  //static String ipserver = "http://192.168.100.8:5000";
  static String ipserver = "https://sistemas.policiacordoba.gov.ar/apppolicia";

  static String apiUrl =
      Platform.isAndroid ? '$ipserver/api/v1' : 'http://localhost:5000/api/v1';
  static String socketUrl =
      Platform.isAndroid ? 'http://$ipserver/chat' : 'http://localhost:3000';
}
