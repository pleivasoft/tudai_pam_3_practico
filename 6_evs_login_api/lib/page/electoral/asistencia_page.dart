import 'package:app_policia/api/api_service.dart';
import 'package:app_policia/api/api_service_action.dart';
import 'package:app_policia/models/empleado.dart';
import 'package:app_policia/models/turno.dart';
import 'package:app_policia/providers/login_form_provider.dart';
import 'package:app_policia/services/auth_service.dart';
import 'package:app_policia/services/elecciones_service.dart';
import 'package:app_policia/services/notifications_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AsistenciaPage extends StatefulWidget {
  AsistenciaPage({Key? key}) : super(key: key);

  @override
  _AsistenciaPageState createState() => _AsistenciaPageState();
}

class _AsistenciaPageState extends State<AsistenciaPage> {
  late AuthService authService;

  final _formKey = GlobalKey<FormState>();
  var _ctrlDNI = TextEditingController();
  var empleado;
  var empleadoTurnos = <Empleado>[];
  var isDatoDisponible = true;
  bool hideError = false;
  bool buscandoTurnosEmpleado = true;
  List<Turno> turnos = <Turno>[];
  var turno;

  @override
  void initState() {
    super.initState();
  }

  @override
  Future<void> didChangeDependencies() async {
    super.didChangeDependencies();
    authService = Provider.of<AuthService>(context);
    _ctrlDNI.text = authService.isToken.nombreUsuario;
    await cargarTurno();
    buscarmiPresentismo();
    //buscarEmpleado();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xff294a70),
          elevation: 0.1,
          title: Text("Registrar asistencia"),
        ),
        body: Form(
          key: _formKey,
          autovalidateMode: AutovalidateMode.always,
          child: Stack(children: <Widget>[
            SingleChildScrollView(
                padding: const EdgeInsets.only(
                    left: 10.0, right: 10.0, top: 10.0, bottom: 65.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      /* TextFormField(
                        controller: _ctrlDNI,
                        scrollPadding: EdgeInsets.only(bottom: 90),
                        keyboardType: TextInputType.number,
                        decoration: const InputDecoration(
                            hintText: 'Escriba un DNI',
                            labelText: 'DNI *',
                            labelStyle: TextStyle(color: Colors.indigo),
                            hintStyle: TextStyle(color: Colors.blue)),
                        validator: validarNumero,
                        onSaved: (newValue) {
                          setState(() {
                            //paciente.telefono = int.tryParse(newValue.toString());
                          });
                        },
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      ElevatedButton(
                        onPressed: buscarEmpleado,
                        child: Text("Buscar empleado"),
                        style: ElevatedButton.styleFrom(
                            minimumSize: Size(double.infinity,
                                35) // put the width and height you want
                            ),
                      ),
                      SizedBox(
                        height: 30,
                      ), */
                      //empleado != null
                      //?
                      Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Center(
                              child: Text("Usuario sesión iniciada",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold)),
                            ),
                            Divider(),
                            RichText(
                              text: TextSpan(
                                  text: "Dni:",
                                  style: textStyleTitulo,
                                  children: [
                                    TextSpan(
                                        text: authService.isToken.nombreUsuario
                                            .toString(),
                                        style: textStyle)
                                  ]),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            RichText(
                              text: TextSpan(
                                  text: "Apellido, Nombre:",
                                  style: textStyleTitulo,
                                  children: [
                                    TextSpan(
                                        text: authService.isToken.apellido
                                                .toString() +
                                            "," +
                                            authService.isToken.nombre
                                                .toString(),
                                        style: textStyle)
                                  ]),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Center(
                              child: Text("Presentismos cargados",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold)),
                            ),
                            Divider(),
                            this.buscandoTurnosEmpleado
                                ? Center(child: CircularProgressIndicator())
                                : Center(
                                    child: Text(empleadoTurnos.isEmpty
                                        ? "No registra asistencia"
                                        : "")),
                            for (var i in empleadoTurnos)
                              Text("-" + findTurno(i.turnoId).nombre,
                                  style: textStyleTitulo),
                            /* SizedBox(
                                    height: 10,
                                  ),
                                  RichText(
                                    text: TextSpan(
                                        text: "Jeraraquia:",
                                        style: textStyleTitulo,
                                        children: [
                                          TextSpan(
                                              text: empleado!.jerarquia,
                                              style: textStyle)
                                        ]),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  RichText(
                                    text: TextSpan(
                                        text: "Dependencia:",
                                        style: textStyleTitulo,
                                        children: [
                                          TextSpan(
                                              text: empleado.dependencia,
                                              style: textStyle)
                                        ]),
                                  ), */
                            Divider(),
                            SizedBox(
                              height: 10,
                            ),
                            DropdownButton<Turno>(
                              value: turno,
                              isExpanded: true,
                              hint: const Text(
                                "Seleccione turno de cobertura",
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.indigo,
                                    fontWeight: FontWeight.bold),
                              ),
                              icon: const Icon(Icons.arrow_downward),
                              elevation: 16,
                              style: const TextStyle(color: Colors.deepPurple),
                              underline: Container(
                                height: 2,
                                color: Color.fromARGB(255, 8, 10, 161),
                              ),
                              onChanged: (Turno? value) {
                                setState(() {
                                  this.turno = value!;
                                });
                              },
                              items: turnos
                                  .map<DropdownMenuItem<Turno>>((Turno value) {
                                return DropdownMenuItem<Turno>(
                                  value: value,
                                  child: Text(
                                    value.nombre.toString(),
                                    style: TextStyle(fontSize: 20),
                                  ),
                                );
                              }).toList(),
                            )
                          ]),
                      SizedBox(
                        height: 30,
                      ),
                    ])),
            Align(
              alignment: Alignment.bottomCenter,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: isDatoDisponible
                        ? Expanded(
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  minimumSize: Size(double.infinity,
                                      48) // put the width and height you want
                                  ),
                              child: Text('Registrar asistencia'),
                              onPressed: () async {
                                setState(() {
                                  hideError = true;
                                  isDatoDisponible = false;
                                });

                                if (turno == null) {
                                  NotificationsService.showSnackbarError(
                                      "Selccione un turno");
                                  setState(() {
                                    isDatoDisponible = true;
                                  });
                                  return;
                                }
                                var eleccionesService =
                                    Provider.of<EleccionesService>(context,
                                        listen: false);
                                var body = {
                                  "escuelaId": eleccionesService.escuela!.id,
                                  "turnoId": turno.turnoId,
                                  "empId": 99,
                                  "dni": _ctrlDNI.text,
                                };
                                bool estado = await ApiService()
                                    .registrarAsistencia(body);
                                if (estado) Navigator.pop(context);
                                setState(() {
                                  isDatoDisponible = true;
                                });
                              },
                            ),
                          )
                        : Padding(
                            padding: EdgeInsets.only(bottom: 10.0),
                            child: CircularProgressIndicator()),
                  ),
                ],
              ),
            )
          ]),
        ));
  }

  get textStyle => TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
        color: Color.fromARGB(172, 21, 17, 250),
      );

  get textStyleTitulo => TextStyle(
        fontSize: 16,
        letterSpacing: 1,
        color: Color.fromARGB(255, 6, 7, 53),
      );

  String? validarNumero(String? value) {
    if (value!.isNotEmpty) {
      String pattern = r'^[\d]{1,2}\.?[\d]{3,3}\.?[\d]{3,3}$';
      //String pattern =
      //r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
      RegExp regExp = new RegExp(pattern);
      return regExp.hasMatch(value)
          ? ""
          : 'El valor ingresado no luce como dni';
    }
    return null;
  }

  String? validarTexto(String? value) {
    if (value == null || value.isEmpty) {
      return 'Ingrese un texto';
    }
    return null;
  }

  void limpiarText() {
    _ctrlDNI.clear();
  }

  void buscarEmpleado() async {
    this.empleado = await ApiAction().buscaEmpleado(_ctrlDNI.text.toString());
    setState(() {});
  }

  void buscarmiPresentismo() async {
    setState(() {
      buscandoTurnosEmpleado = true;
    });
    var eleccionesService =
        Provider.of<EleccionesService>(context, listen: false);
    var body = {
      "escuelaId": eleccionesService.escuela!.id,
      "dni": _ctrlDNI.text,
    };
    this.empleadoTurnos = await ApiAction().obtenerPersonalEscuelaPorDni(body);
    setState(() {
      buscandoTurnosEmpleado = false;
    });
  }

  Future<void> cargarTurno() async {
    this.turnos = await ApiAction().obtenerTurnos();
    setState(() {});
  }

  findTurno(int? id) => turnos.firstWhere((turno) => turno.turnoId == id);
}
