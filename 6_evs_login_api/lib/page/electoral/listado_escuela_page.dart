import 'package:app_policia/models/escuela.dart';
import 'package:app_policia/screens/home_screen.dart';
import 'package:app_policia/services/auth_service.dart';
import 'package:app_policia/services/elecciones_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ListadoEscuelasPage extends StatefulWidget {
  ListadoEscuelasPage({Key? key}) : super(key: key);

  @override
  _ListadoEscuelasPagetate createState() => _ListadoEscuelasPagetate();
}

class _ListadoEscuelasPagetate extends State<ListadoEscuelasPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController _filter = new TextEditingController();

  String _searchText = "";
  Icon _searchIcon = new Icon(Icons.search);
  var colorTextPri = Colors.blue[900];

  List<Escuela> escuelaFiltro = <Escuela>[];

  late EleccionesService eleccionesService;

  _ListadoEscuelasPagetate() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";
          escuelaFiltro = eleccionesService.escuelas;
        });
      } else {
        setState(() {
          _searchText = _filter.text;
        });
      }
    });
  }

  Widget _appBarTitle = new Text(
    'Escuelas',
    style: new TextStyle(color: Colors.white),
  );

  get topAppBar => AppBar(
        backgroundColor: Color(0xff294a70),
        automaticallyImplyLeading: false,
        elevation: 0.1,
        title: _appBarTitle,
        actions: <Widget>[
          IconButton(icon: _searchIcon, onPressed: _searchPressed),
          IconButton(
            icon: Icon(Icons.logout),
            onPressed: () async {
              eleccionesService.limpiar();
              final authService =
                  Provider.of<AuthService>(context, listen: false);
              authService.logout();
              Navigator.pushNamedAndRemoveUntil(
                context,
                'login',
                (route) => false,
              );
            },
          ),
        ],
      );

  void _searchPressed() {
    setState(() {
      if (this._searchIcon.icon == Icons.search) {
        this._searchIcon = new Icon(Icons.close);
        this._appBarTitle = new TextField(
          controller: _filter,
          autofocus: true,
          style: new TextStyle(
            color: Colors.white,
          ),
          decoration: new InputDecoration(
              prefixIcon: new Icon(
                Icons.search,
                color: Colors.white,
              ),
              hintText: "Buscar..",
              hintStyle: TextStyle(color: Colors.white),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide.none),
              contentPadding: EdgeInsets.zero),
        );
      } else {
        this._searchIcon = new Icon(Icons.search);
        this._appBarTitle = new Text('Escuelas');
        escuelaFiltro = eleccionesService.escuelas;
        _filter.clear();
      }
    });
  }

  @override
  Future<void> didChangeDependencies() async {
    super.didChangeDependencies();
    eleccionesService = Provider.of<EleccionesService>(context, listen: false);
    getEscuelas();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: topAppBar,
        body: RefreshIndicator(
            child: Container(child: _construirLista()),
            onRefresh: getEscuelas));
  }

  Widget _construirLista() {
    if (_searchText.isNotEmpty) {
      escuelaFiltro = eleccionesService.escuelas
          .where((c) =>
              (c.escuela.toLowerCase().contains(_searchText.toLowerCase()) ||
                  c.escuela.toLowerCase().contains(_searchText.toLowerCase())))
          .toList();
    }

    return escuelaFiltro.isNotEmpty
        ? ListView.builder(
            padding: EdgeInsets.only(bottom: 60.0),
            itemCount: escuelaFiltro.length,
            itemBuilder: (BuildContext context, int index) {
              return crearCard(escuelaFiltro[index]);
            },
          )
        : LayoutBuilder(
            builder: (context, constraints) {
              return SingleChildScrollView(
                physics: const AlwaysScrollableScrollPhysics(),
                child: SizedBox(
                  height: constraints.maxHeight,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Text('No tiene escuela asignada'),
                        Text('Desliza hacia abajo para actualizar.'),
                      ],
                    ),
                  ),
                ),
              );
            },
          );
  }

  Card crearCard(Escuela escuela) {
    return Card(
      shape: Border(left: BorderSide(color: Colors.blueAccent, width: 5)),
      elevation: 8.0,
      margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      child: Container(
        child: crearListTile(escuela),
      ),
    );
  }

  ListTile crearListTile(Escuela escuela) => ListTile(
        contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 1.0),
        leading: Icon(
          Icons.how_to_vote_rounded,
          color: Colors.blueAccent,
          size: 40.0,
        ),
        title: Row(
          children: [
            Expanded(
                flex: 3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      escuela.escuela,
                      style: TextStyle(
                          color: colorTextPri, fontWeight: FontWeight.bold),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
                      child: Text(
                        escuela.dependencia,
                        style: TextStyle(color: colorTextPri),
                      ),
                    ),
                  ],
                )),
          ],
        ),
        subtitle: RichText(
          text: TextSpan(
            text: "Estado: ",
            children: [
              TextSpan(
                text: escuela.estado,
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.blueAccent,
                ),
              )
            ],
            style: TextStyle(
              fontSize: 20,
              color: Colors.black,
            ),
          ),
        ),
        onTap: () async {
          eleccionesService.escuela = escuela;
          await Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => HomeScreen()));
          getEscuelas();
        },
      );

  Future<void> getEscuelas() async {
    await eleccionesService.cargarEscuela();
    if (eleccionesService.escuelas.length == 1) {
      Navigator.pushNamedAndRemoveUntil(
        context,
        'home',
        (route) => false,
      );
    }
    escuelaFiltro.clear();
    setState(() {
      escuelaFiltro = eleccionesService.escuelas;
    });
  }
}
