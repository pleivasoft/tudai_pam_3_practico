//import 'package:json_annotation/json_annotation.dart';
part 'refresh_token_body.g.dart';

//@JsonSerializable()
class RefreshTokenBody {
  
  String? token;
  String? refreshToken;
  String? tokenNotificacion;

  RefreshTokenBody(this.token, this.refreshToken,this.tokenNotificacion);

  factory RefreshTokenBody.fromJson(Map<String, dynamic> json) => _$RefreshTokenBodyFromJson(json);

  Map<String, dynamic> toJson() => _$RefreshTokenBodyToJson(this);

  @override
  String toString() {
    return 'RefreshTokenBody{grantType: $token, refreshToken: $refreshToken,  tokenNotificacion: $tokenNotificacion}';
  }
}