class Turno {
  int? turnoId;
  String? nombre;
  Turno.empty();
  Turno({this.turnoId, this.nombre});

  Turno.fromJson(Map<String, dynamic> json) {
    turnoId = json['turnoId'];
    nombre = json['nombre'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['turnoId'] = this.turnoId;
    data['nombre'] = this.nombre;
    return data;
  }
}
